
var box = new Window('dialog', "Customize-inator!");
  var mapBtn = box.add('button',undefined, "Custom Maps",undefined);
  var photoBtn = box.add('button',undefined, "Custom Photos",undefined);
//========================================================================================================
    mapBtn.addEventListener("mouseover", function(k){
        helper(k,'Chạy ra file mapMask.json\rChú ý đúng đường dẫn svg trong folder images. Điều chỉnh vị trí của marker trên map bằng giá trị x y')
    });
    //==================================================================mapBtn.addEventListener("mouseover", function(k){
    photoBtn.addEventListener("mouseover", function(k){
        helper(k,'Chạy ra file mask.json\rChú ý đúng đường dẫn svg trong folder images.')
    });
//=========================================================================================================
    mapBtn.onClick = function(){
        box.close();
        mapMask();
    };
    photoBtn.onClick = function(){
        box.close();
        photoMask();
    };
//=========================================================================================================
box.show()
//========================================================================================================
//========================================================================================================
function photoMask(){
      var doc = activeDocument;
      //doc.rulerUnits = RulerUnits.Pixels
      var docW = doc.artboards[0].artboardRect[2]*2
      var docH = doc.artboards[0].artboardRect[1]*2
      var data = new Array();
          var prosLyrs = new Array();
          for( i=0;i<doc.layers[0].pageItems.length;i++ ){
                  prosLyrs.push(doc.layers[0].pageItems[i].name);
              };

          for( i=0;i<prosLyrs.length;i++ ){
                  data.push(compose( prosLyrs[i] )+'\n');
          };
      saveAsTextFile(activeDocument.path,data.join(''))
      ///////

      //////////

      function compose(itemName){
          var currData = new Array();
          var currItem = doc.layers[0].pageItems.getByName(itemName);
              currData.push('        {'+
                            '\r            \"left\":'+Calc(docW,currItem.visibleBounds[0])+','+
                            '\r            \"top\":'+Calc(docH,-currItem.visibleBounds[1])+','+
                            '\r            \"width\":'+Math.ceil(currItem.width)+','+
                            '\r            \"height\":'+Math.ceil(currItem.height)+','+
                            '\r            \"opacity\":1'+','+
                            '\r            \"visible\":true'+','+
                            '\r            \"type\":"mask"'+','+
                            '\r            \"image\":\"images\/'+currItem.name+'.svg\"'+
                            '\n        },'
              );
          return currData
      }

      function Calc(val1,val2){
          var Calculated = (val1/2)+Math.ceil( val2 );
          return Calculated;
      };
      function saveAsTextFile(filePath,content) {
          var saveFile = new File(filePath+"/mask.json");
          saveFile.encoding = "UTF8";
          saveFile.open("w");
          if (saveFile.error != "")
              return saveFile.error;

          saveFile.write(content);
          if (saveFile.error != "")
              return saveFile.error;

          saveFile.close();
          if (saveFile.error != "")
              return saveFile.error;

          return "";
      };//func
};
//========================================================================================================
//========================================================================================================
//========================================================================================================
function mapMask(){
    var doc = activeDocument;
    //doc.rulerUnits = RulerUnits.Pixels
    var docW = doc.artboards[0].artboardRect[2]*2
    var docH = doc.artboards[0].artboardRect[1]*2
    var data = new Array();
        var prosLyrs = new Array();
        for( i=0;i<doc.layers[0].pageItems.length;i++ ){
                prosLyrs.push(doc.layers[0].pageItems[i].name);
            };

        for( i=0;i<prosLyrs.length;i++ ){
                data.push(compose( prosLyrs[i] )+'\n');
        };
    saveAsTextFile(activeDocument.path,data.join(''))
    ///////

    //////////

    function compose(itemName){
        var currData = new Array();
        var currItem = doc.layers[0].pageItems.getByName(itemName);
            currData.push('        {'+
                          '\n            \"left\":'+Calc(docW,currItem.visibleBounds[0])+','+
                          '\n            \"top\":'+Calc(docH,-currItem.visibleBounds[1])+','+
                          '\n            \"width\":'+Math.ceil(currItem.width)+','+
                          '\n            \"height\":'+Math.ceil(currItem.height)+','+
                          '\n            \"opacity\":1'+','+
                          '\n            \"visible\":true'+','+
                          '\n            \"type\":\"map-mask[id:1,x:'+(Math.ceil(currItem.width)-340)/2+',y:'+(Math.ceil(currItem.height)-340)/2+',index:1,label:'+doc.layers[0].name+']\",'+
                          '\n            \"image\":\"images\/'+currItem.name+'.svg\"'+
                          '\n        },'
            );
        return currData
    }

    function Calc(val1,val2){
        var Calculated = (val1/2)+Math.ceil( val2 );
        return Calculated;
    };
    function saveAsTextFile(filePath,content) {
        var saveFile = new File(filePath+"/mapMask.json");
        saveFile.encoding = "UTF8";
        saveFile.open("w");
        if (saveFile.error != "")
            return saveFile.error;

        saveFile.write(content);
        if (saveFile.error != "")
            return saveFile.error;

        saveFile.close();
        if (saveFile.error != "")
            return saveFile.error;

        return "";
    };//func
};
function helper(shiftChk,text){
    if(shiftChk.shiftKey){
      alert(text)
    }
};