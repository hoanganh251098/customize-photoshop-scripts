﻿var doc = activeDocument;
// png options;
pngOpts = new PNGSaveOptions;
pngOpts.compression = 9;
pngOpts.interlaced = false;

// jpg options;
jpgOpts = new JPEGSaveOptions();
jpgOpts.quality = 8; // enter number or create a variable to set quality
jpgOpts.embedColorProfile = false;
jpgOpts.matte = MatteType.NONE
jpgOpts.formatOptions = FormatOptions.STANDARDBASELINE;

// PSD Options;
psdOpts = new PhotoshopSaveOptions();
psdOpts.embedColorProfile = true;
psdOpts.alphaChannels = true;
psdOpts.layers = true;
psdOpts.spotColors = true;

// ////////////////
var cleanScript = "#include" + File("//192.168.1.111/share/scripts/clean.jsx").fullName
eval(cleanScript);
// ///////////////////
if (app.documents.length > 0 && app.activeDocument.name !== "process.psd") {
    var savePro = confirm('Save Process.psd ??')
    if (savePro == true) {
        app.activeDocument.saveAs(File(activeDocument.path + "/process.psd"), psdOpts, false);
    } else {}
}
var box = new Window('dialog', "Customize-inator!");
var mainBtn = box.add('button', undefined, "Main Module", undefined);
var bgColorBtn = box.add('button', undefined, "Background Color", undefined);
var bgImageBtn = box.add('button', undefined, "Background Image", undefined);
var textBtn = box.add('button', undefined, "Text", undefined);
var maskBtn = box.add('button', undefined, "Mask", undefined);
var mapBtn = box.add('button', undefined, "Map", undefined);
var songBtn = box.add('button', undefined, "Song (heart)", undefined);
var songRectBtn = box.add('button', undefined, "Song (rect)", undefined);
var sampleBtn = box.add('button', undefined, "Open Sample PSD File", undefined);
var customizeFolBtn = box.add('button', undefined, "Open Customize Folder", undefined);

// //////////////////////////
// =================================================================================================================================
// =================================================================================================================================
// =================================================================================================================================
// ===============================================================================================
mainBtn.addEventListener("mouseover", function (k) {
    helper(k, 'Chạy đầu tiên. Ra file data.json. Dùng định nghĩa kích thước canvas, làm nền tảng thêm các thành phần khác')
});
// ===============================================================================================
bgColorBtn.addEventListener("mouseover", function (k) {
    helper(k, 'Chạy với group backgroundColor. Ra file bgColor.json, khi làm mẫu có tuỳ chọn màu nền (Tham khảo file mẫu)')
});
// ===============================================================================================
bgImageBtn.addEventListener("mouseover", function (k) {
    helper(k, 'Chạy với group backgroundImage. Ra file bgImage.json (Tham khảo file mẫu)')
});
// ===============================================================================================
textBtn.addEventListener("mouseover", function (k) {
    helper(k, 'Chạy với group text (chứa các group con). Ra file text.json (Tham khảo file mẫu)')
});
// ===============================================================================================
maskBtn.addEventListener("mouseover", function (k) {
    helper(k, 'Chạy với group mask. Ra file mask.psd để xử lý tiếp trên Illustrator (Tham khảo file mẫu)')
});
// ===============================================================================================
mapBtn.addEventListener("mouseover", function (k) {
    helper(k, 'Chạy với group map. Ra file map.json và map.psd để xử lý tiếp trên Illustrator (Tham khảo file mẫu)\rLưu ý cần có smartObject của file marker')
});
// ===============================================================================================
songBtn.addEventListener("mouseover", function (k) {
    helper(k, 'Chạy với group song. Ra file song.json (Tham khảo file mẫu).\rChú ý cần đủ các thành phần. Nếu không sử dụng song title thì set opacity 0 trong file json')
});
// =================================================================================================================================
// =================================================================================================================================
// =================================================================================================================================
var imageFolder = Folder(doc.path.fullName.slice(0, doc.path.fullName.lastIndexOf('/')) + '/images')

// //////////////////

mainBtn.onClick = function () {
    box.close();
    main();
};

bgColorBtn.onClick = function () {
    box.close();
    bgColor();
};

bgImageBtn.onClick = function () {
    box.close();
    bgImage();
};

textBtn.onClick = function () {
    box.close();
    text();
};

maskBtn.onClick = function () {
    box.close();
    mask();
};

mapBtn.onClick = function () {
    box.close();
    map();
};

songBtn.onClick = function () {
    box.close();
    song();
};
sampleBtn.onClick = function () {
    box.close();
    app.open(File('//192.168.1.111/share/scripts/Customize/sample.psd'));
};
customizeFolBtn.onClick = function () {
    box.close();
    Folder('//192.168.1.111/share/scripts/Customize').execute();
};

box.show();

// //////////////////////

function main() {
    var data = "{" + "\r    \"document\":{" + "\r        \"width\":" + doc.width.value + "," + "\r        \"height\":" + doc.height.value + "," + "\r        \"resolution\":" + doc.resolution + "\r    }," + "\r    \"children\":[" + "\r" + "\r    ]" + "\r}";
    writeToFile('data', data);
};

// /////////////////////

function bgImage() {
    var actLyrSet = doc.layerSets.getByName('backgroundImage');
    doc.saveAs(File(imageFolder + "/background.png"), pngOpts, true);
    app.open(File(imageFolder + "/background.png"));
    activeDocument.width > activeDocument.height ? activeDocument.resizeImage(1500, null, 72, ResampleMethod.BICUBIC) : activeDocument.resizeImage(null, 1500, 72, ResampleMethod.BICUBIC);
    activeDocument.saveAs(File(activeDocument.path + "/preview-background.png"), pngOpts, true);
    activeDocument.close(SaveOptions.DONOTSAVECHANGES);
    var data = "        {" + "\r            \"left\":0," + "\r            \"top\":0," + "\r            \"width\":" + doc.width.value + "," + "\r            \"height\":" + doc.height.value + "," + "\r            \"opacity\":" + parseInt(actLyrSet.opacity / 100) + "," + "\r            \"visible\":true," + "\r            \"blendMode\":\"normal\"," + "\r            \"type\":\"fg\"," + "\r            \"image\":\"images/background.png\"," + "\r            \"preview\":\"images/preview-background.png\"" + "\r        },";
    writeToFile('bgImage', data);
};

// /////////////////////

function text() {
    var actLyrSet = doc.layerSets.getByName('text');
    var finalData = new Array();
    for (i = 0; i < actLyrSet.layerSets.length; i ++) {
        var tempLyrSet = actLyrSet.layerSets[i];
        for (j = 0; j < tempLyrSet.layers.length; j ++) {
            tempChildLyr = tempLyrSet.layers[j];
            if (tempChildLyr.kind == LayerKind.TEXT) {
                activeDocument.activeLayer = tempChildLyr;
                var boundsLyr = tempLyrSet.layers.getByName('box');
                var curData = txtDataHelper(boundsLyr.bounds[0].value, boundsLyr.bounds[1].value, boundsLyr.bounds[2].value - boundsLyr.bounds[0].value, boundsLyr.bounds[3].value - boundsLyr.bounds[1].value, tempLyrSet.name.indexOf('*') > -1 ? true : false, activeDocument.activeLayer.name, activeDocument.activeLayer.textItem.contents);
                finalData.push(curData);
            };
        };
    };
    // //////////////////

    writeToFile('text', finalData.join('\r'));
    // //////////////////////
    function txtDataHelper(txtLeft, txtTop, txtWidth, txtHeight, require, label, content) {
        var data = "        {" + "\r            \"left\":" + txtLeft + "," + "\r            \"top\":" + txtTop + "," + "\r            \"width\":" + txtWidth + "," + "\r            \"height\":" + txtHeight + "," + "\r            \"opacity\":" + activeDocument.activeLayer.opacity / 100 + "," + "\r            \"visible\":true," + "\r            \"lineHeight\":1.1," + "\r            \"type\":\"text[required:" + require + ",label:" + label + ",valign:middle]\"," + "\r            \"rotate\":0," + "\r            \"text\":{" + "\r                \"value\":\"" + content + "\"," + "\r                \"font\":{" + "\r                    \"name\":\"" + activeDocument.activeLayer.textItem.font + "\"," + "\r                    \"sizes\":[" + "\r                        " + Math.round(fontSize() * 4.116) + "\r                    ]," + "\r                    \"colors\":[" + "\r                      [" + "\r                        " + Math.round(activeDocument.activeLayer.textItem.color.rgb.red) + "," + "\r                        " + Math.round(activeDocument.activeLayer.textItem.color.rgb.green) + "," + "\r                        " + Math.round(activeDocument.activeLayer.textItem.color.rgb.blue) + "," + "\r                        255" + "\r                      ]" + "\r                    ]," + "\r                    \"alignment\":[" + "\r                      \"center\"" + "\r                    ]," + "\r                    \"path\":\"fonts/\"" + "\r                }," + "\r                \"box\":{" + "\r                    \"width\":" + txtWidth + "," + "\r                    \"height\":" + txtHeight + "\r                }" + "\r            }" + "\r        },";
        return data
    };

};

// /////////////////////

function mask() {
    var maskDoc = app.documents.add(doc.width, doc.height, doc.resolution)
    activeDocument = doc;
    var actLyrSet = doc.layerSets.getByName('mask');
    actLyrSet.duplicate(maskDoc, ElementPlacement.INSIDE);
    activeDocument = maskDoc;
    activeDocument.layerSets[0].name = "Custom Photo"
    activeDocument.saveAs(File(doc.path + "/mask.psd"), psdOpts, false);
    activeDocument.backgroundLayer.remove();
    activeDocument.close(SaveOptions.DONOTSAVECHANGES);
};

// //////////////////////

function map() {
    var mapDoc = app.documents.add(doc.width, doc.height, doc.resolution);
    activeDocument = doc;
    var actLyrSet = doc.layerSets.getByName('map');
    actLyrSet.duplicate(mapDoc, ElementPlacement.INSIDE);
    activeDocument = mapDoc;
    activeDocument.layerSets[0].name = "Custom Map"
    activeDocument.saveAs(File(doc.path + "/map.psd"), psdOpts, false);
    activeDocument.backgroundLayer.remove();
    for (i = 0; i < activeDocument.layerSets[0].layers.length; i ++) {
        if (activeDocument.layerSets[0].layers[i].name.indexOf('marker') > -1) {
            var markerLyr = activeDocument.layerSets[0].layers[i];
            var markerFile = File("//192.168.1.111/share/scripts/Customize/" + markerLyr.name + ".png");
            markerFile.copy(imageFolder + '/' + markerFile.name);
            var markerDimm = [
                markerLyr.bounds[2] - markerLyr.bounds[0],
                markerLyr.bounds[3] - markerLyr.bounds[1],
                markerLyr.bounds[0],
                markerLyr.bounds[1]
            ];
            break;
        }
    };
    activeDocument.close(SaveOptions.DONOTSAVECHANGES);

    var data = "\r        {" + "\r            \"left\":" + markerDimm[2].value + "," + "\r            \"top\":" + markerDimm[3].value + "," + "\r            \"width\":" + markerDimm[0].value + "," + "\r            \"height\":" + markerDimm[1].value + "," + "\r            \"opacity\":" + parseInt(actLyrSet.opacity / 100) + "," + "\r            \"visible\":true," + "\r            \"blendMode\":\"normal\"," + "\r            \"type\":\"marker[id:1]\"," + "\r            \"image\":\"images/" + markerFile.name + "\"" + "\r        }," + "\r        {" + "\r            \"left\":0," + "\r            \"top\":0," + "\r            \"width\":" + doc.width.value + "," + "\r            \"height\":" + doc.height.value + "," + "\r            \"opacity\":" + parseInt(actLyrSet.opacity / 100) + "," + "\r            \"visible\":true," + "\r            \"blendMode\":\"normal\"," + "\r            \"type\":\"map[style:mapbox-ckhzqo9bu1ybf19pu59w56it6,zoom:11,align:middle]\"" + "\r        },";
    writeToFile('map', data);
}

// /////////////////////

function song() {
    var actLyrSet = doc.layerSets.getByName('song');
    var heartShapeFile = File("//192.168.1.111/share/scripts/Customize/1591936194287491d8.svg")
    heartShapeFile.copy(imageFolder + '/' + heartShapeFile.name);
    var songmaskLyr = actLyrSet.layers.getByName('songmask');
    var songmaskData = "        {" + "\r            \"left\":" + songmaskLyr.bounds[0].value + "," + "\r            \"top\":" + songmaskLyr.bounds[1].value + "," + "\r            \"width\":" + Math.round(songmaskLyr.bounds[2].value - songmaskLyr.bounds[0].value) + "," + "\r            \"height\":" + Math.round(songmaskLyr.bounds[3].value - songmaskLyr.bounds[1].value) + "," + "\r            \"opacity\":" + songmaskLyr.opacity / 100 + "," + "\r            \"visible\":true," + "\r            \"type\":\"song-mask\"," + "\r            \"image\":\"images/1591936194287491d8.svg\"" + "\r        },";

    // //////////////////////////////
    var songlyricsLyr = actLyrSet.layers.getByName('songlyrics');
    activeDocument.activeLayer = songlyricsLyr;
    var songlyricsData = "        {" + "\r            \"left\":" + songmaskLyr.bounds[0].value + "," + "\r            \"top\":" + songmaskLyr.bounds[1].value + "," + "\r            \"width\":" + Math.round(songmaskLyr.bounds[2].value - songmaskLyr.bounds[0].value) + "," + "\r            \"height\":" + Math.round(songmaskLyr.bounds[3].value - songmaskLyr.bounds[1].value) + "," + "\r            \"opacity\":" + songlyricsLyr.opacity / 100 + "," + "\r            \"visible\":true," + "\r            \"lineHeight\":1.0," + "\r            \"type\":\"text[module:song_lyrics]\"," + "\r            \"rotate\":0," + "\r            \"text\":{" + "\r                \"value\":\"" + songlyricsLyr.textItem.contents.split("\r").join(" ") + "\"," + "\r                \"font\":{" + "\r                    \"name\":\"" + songlyricsLyr.textItem.font + "\"," + "\r                    \"sizes\":[" + "\r                        " + Math.round(fontSize() * 4.116) + "\r                    ]," + "\r                    \"colors\":[" + "\r                      [" + "\r                        " + Math.round(songlyricsLyr.textItem.color.rgb.red) + "," + "\r                        " + Math.round(songlyricsLyr.textItem.color.rgb.green) + "," + "\r                        " + Math.round(songlyricsLyr.textItem.color.rgb.blue) + "," + "\r                        255" + "\r                      ]" + "\r                    ]," + "\r                    \"alignment\":[" + "\r                      \"left\"" + "\r                    ]," + "\r                    \"path\":\"fonts/\"" + "\r                }" + "\r            }" + "\r        },";

    // /////////////////////////////
    var songtitleLyr = actLyrSet.layers.getByName('songtitle');
    activeDocument.activeLayer = songtitleLyr;
    var songtitleBounds = actLyrSet.layers.getByName('songtitlebox');
    var songtitleData = "        {" + "\r            \"left\":" + songtitleBounds.bounds[0].value + "," + "\r            \"top\":" + songtitleBounds.bounds[1].value + "," + "\r            \"width\":" + Math.round(songtitleBounds.bounds[2].value - songtitleBounds.bounds[0].value) + "," + "\r            \"height\":" + Math.round(songtitleBounds.bounds[3].value - songtitleBounds.bounds[1].value) + "," + "\r            \"opacity\":" + songtitleLyr.opacity / 100 + "," + "\r            \"visible\":true," + "\r            \"type\":\"text[module:song_title,valign:middle]\"," + "\r            \"rotate\":0," + "\r            \"text\":{" + "\r                \"value\":\"" + songtitleLyr.textItem.contents + "\"," + "\r                \"font\":{" + "\r                    \"name\":\"" + songtitleLyr.textItem.font + "\"," + "\r                    \"sizes\":[" + "\r                        " + Math.round(fontSize() * 4.116) + "\r                    ]," + "\r                    \"colors\":[" + "\r                      [" + "\r                        " + Math.round(songtitleLyr.textItem.color.rgb.red) + "," + "\r                        " + Math.round(songtitleLyr.textItem.color.rgb.green) + "," + "\r                        " + Math.round(songtitleLyr.textItem.color.rgb.blue) + "," + "\r                        255" + "\r                      ]" + "\r                    ]," + "\r                    \"alignment\":[" + "\r                      \"center\"" + "\r                    ]," + "\r                    \"path\":\"fonts/\"" + "\r                }," + "\r                \"box\":{" + "\r                    \"width\":" + Math.round(songtitleBounds.bounds[2].value - songtitleBounds.bounds[0].value) + "," + "\r                    \"height\":" + Math.round(songtitleBounds.bounds[3].value - songtitleBounds.bounds[1].value) + "\r                }" + "\r            }" + "\r        },";

    var data = songmaskData + "\r" + songlyricsData + "\r" + songtitleData
    writeToFile('song', data);
};


// /////////////////

function bgColor() {
    var actLyrSet = doc.layerSets.getByName('backgroundColor');
    var colorAr = new Array();
    for (i = 0; i < actLyrSet.layers.length; i ++) {
        actLyrSet.layers[i].visible = false;
    };

    for (i = 0; i < actLyrSet.layers.length; i ++) {
        actLyrSet.layers[i].visible = true;
        colorAr.push(ColorSampler().toLowerCase());
        actLyrSet.layers[i].visible = false;
    };

    doc.colorSamplers.removeAll();

    for (i = 0; i < actLyrSet.layers.length; i ++) {
        actLyrSet.layers[i].visible = true;
    };

    var data = "        {" + "\r            \"left\":0," + "\r            \"top\":0," + "\r            \"width\":" + doc.width.value + "," + "\r            \"height\":" + doc.height.value + "," + "\r            \"opacity\":" + parseInt(actLyrSet.opacity / 100) + "," + "\r            \"visible\":true," + "\r            \"blendMode\":\"normal\"," + "\r            \"type\":\"bgcolor[label:Choose Background Color,clickable:true,color:" + colorAr.slice(0, 1) + ",changecolor:" + colorAr.join('/') + "]\"" + "\r        },";
    writeToFile('bgColor', data);

};

// /////////////////////
function writeToFile(curModule, dData) {
    var jFile = new File(doc.path + '/' + curModule + '.json');
    jFile.open('w');
    jFile.writeln(dData);
    jFile.close();
    // file.execute()
} //

function ColorSampler() {
    doc.colorSamplers.removeAll();
    var curColor = doc.colorSamplers.add([
        doc.width / 2,
        doc.height / 2
    ]);
    return curColor.color.rgb.hexValue;
}


function fontSize() {
    var a = new UnitValue(getTextSize(), 'pt')
    return a
};

function getTextSize() {
    var size = activeDocument.activeLayer.textItem.size;

    var r = new ActionReference();
    r.putProperty(stringIDToTypeID("property"), stringIDToTypeID("textKey"));
    r.putEnumerated(stringIDToTypeID("layer"), stringIDToTypeID("ordinal"), stringIDToTypeID("targetEnum"));

    var yy = 1;
    var yx = 0;

    try {
        var transform = executeActionGet(r).getObjectValue(stringIDToTypeID("textKey")).getObjectValue(stringIDToTypeID("transform"));
        yy = transform.getDouble(stringIDToTypeID("yy"));
        yx = transform.getDouble(stringIDToTypeID("yx"));
    } catch (e) {}

    var coeff = Math.sqrt(yy * yy + yx * yx);
    // alert(size*coeff)
    return size * coeff;
};
function helper(shiftChk, text) {
    if (shiftChk.shiftKey) {
        alert(text)
    }
};
