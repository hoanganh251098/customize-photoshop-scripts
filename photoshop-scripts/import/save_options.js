
// png options;
pngOpts = new PNGSaveOptions();
pngOpts.compression = 0;
pngOpts.interlaced = false;

// jpg options;
jpgOpts = new JPEGSaveOptions();
jpgOpts.quality = 8; // enter number or create a variable to set quality
jpgOpts.embedColorProfile = false;
jpgOpts.matte = MatteType.NONE
jpgOpts.formatOptions = FormatOptions.STANDARDBASELINE;

// PSD Options;
psdOpts = new PhotoshopSaveOptions();
psdOpts.embedColorProfile = true;
psdOpts.alphaChannels = true;
psdOpts.layers = true;
psdOpts.spotColors = true;

