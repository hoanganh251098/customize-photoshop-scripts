/* Helpers */
writeText = function writeText(path, data) {
    try {
        const f = new File(path);
        f.open("w");
        f.encoding = "UTF-8";
        f.write(data);
        f.close();
    } catch (e) {
        alert("Error: " + e.message);
    }
};

readFile = function (path) {
    try {
        const f = new File(path);
        f.open("r");
        f.encoding = "UTF-8";
        var content = f.read();
        f.close();
        return content;
    } catch (e) {
        alert("Error: " + e.message);
    }
};

appendText = function appendText(path, data) {
    try {
        const f = new File(path);
        f.open("a");
        f.encoding = "UTF-8";
        f.write(data);
        f.close();
    } catch (e) {
        alert("Error: " + e.message);
    }
};

clearLog = function clearLog() {
    writeText(cwd + "/log.txt", "");
};

log = function log(text) {
    appendText(cwd + "/log.txt", text + "\n");
};

logjson = function logjson(obj) {
    log(JSON.stringify(obj, undefined, 2));
};

toArray = function (obj) {
    var result = [];
    for (var i = 0; i < obj.length; ++i) {
        result.push(obj[i]);
    }
    return result;
};

getLayerinfo = function (layer) {
    var width, height;
    try {
        width = layer.width.value;
        height = layer.height.value;
    } catch (e) {
        width = layer.bounds[2].value - layer.bounds[0].value;
        height = layer.bounds[3].value - layer.bounds[1].value;
    }
    var layername = "",
        layertype = "",
        layerkind = "";
    try {
        layername = layer.name;
    } catch (e) {}
    try {
        layertype = layer.typename;
    } catch (e) {}
    try {
        layerkind = "" + layer.kind;
    } catch (e) {}
    return {
        name: layername,
        type: layertype,
        kind: layerkind,
        left: layer.bounds[0].value,
        top: layer.bounds[1].value,
        width: width,
        height: height,
        rotate: 0,
        opacity: layer.opacity / 100,
        visible: true,
        blendMode: "normal",
    };
};

query = function query(q, parent) {
    var layerQueries = q.split(" ").filter(function (v) {
        return v != "";
    });
    var parsedQueries = layerQueries.map(function (layerQuery) {
        var metaraw = layerQuery.match(/\[.*/g);
        if (metaraw) {
            metaraw = metaraw[0];
        } else {
            metaraw = "";
        }
        var type = layerQuery.replace(metaraw, "");
        var meta = {};
        var metaMatch = metaraw.match(/[a-zA-Z0-9_-]+\=[\/$a-zA-Z0-9_-\s]+/g);
        if (metaMatch) {
            metaMatch.forEach(function (m) {
                meta[m.split("=")[0]] = m.split("=")[1];
            });
        }
        return {
            type: type || "*",
            meta: meta,
        };
    });
    function _query(layers, remaining) {
        if (
            remaining.length == 0 ||
            layers.length == 0 ||
            remaining[0].type == ""
        )
            return layers;
        var query = remaining[0];
        function getSublayers(layers) {
            var subLayers = [];
            layers.forEach(function (layer) {
                try {
                    subLayers = subLayers.concat(toArray(layer.layers));
                } catch (e) {}
            });
            return subLayers;
        }
        function checkLayerMatch(layer) {
            var typeMatchers = {
                group: function (layer) {
                    return layer.typename == "LayerSet";
                },
                text: function (layer) {
                    return (
                        layer.typename == "ArtLayer" &&
                        layer.kind == LayerKind.TEXT
                    );
                },
                shape: function (layer) {
                    return (
                        layer.typename == "ArtLayer" &&
                        layer.kind == LayerKind.SOLIDFILL
                    );
                },
                normal: function (layer) {
                    return (
                        layer.typename == "ArtLayer" &&
                        layer.kind == LayerKind.NORMAL
                    );
                },
                "*": function (layer) {
                    return true;
                },
            };
            var matcher = typeMatchers[query.type];
            var matchType = !!matcher && matcher(layer);
            if (!matchType) return;
            if (query.meta.name) {
                var replacedName = layer.name.replace(" ", "_");
                while (true) {
                    var before = replacedName;
                    replacedName = replacedName.replace(" ", "_");
                    if (before == replacedName) break;
                }
                var matchName = replacedName == query.meta.name;
                if (!matchName) return;
            }
            return true;
        }
        var matchedLayers = [];
        function findMatches(layers) {
            layers.forEach(function (layer) {
                if (!layer.typename) return;
                if (layer.typename && checkLayerMatch(layer)) {
                    matchedLayers.push(layer);
                }
                findMatches(getSublayers([layer]));
            });
        }
        findMatches(layers);
        if (remaining.length == 1) {
            return matchedLayers;
        }
        return _query(getSublayers(matchedLayers), remaining.slice(1));
    }
    if (!parent) parent = activeDocument;
    return _query(toArray(parent.layers), parsedQueries);
};

queryChildren = function queryChildren(q, parent) {
    var matches = query(q, parent);
    return matches.filter(function (m) {
        return m.parent == parent;
    });
};

LoopLimiter = function (limit) {
    var count = 0;
    var alerted = false;
    return function () {
        count += 1;
        var isLimitBroke = count > limit;
        if (isLimitBroke && !alerted) {
            alert("Loop limit exceeded.");
            alerted = true;
        }
        return !isLimitBroke;
    };
};

uuidv4 = function () {
    return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace( // what?
        /[xy]/g,
        function (c) {
            var r = (Math.random() * 16) | 0,
                v = c == "x" ? r : (r & 0x3) | 0x8; // how?
            return r.toString(16);
        }
    );
};

levenshteinDistance = function (str1, str2) {
    if (!str1) str1 = "";
    if (!str2) str2 = "";
    var track = Array(str2.length + 1)
        .fill(null)
        .map(function () {
            return Array(str1.length + 1).fill(null);
        }); // matrix creation
    for (var i = 0; i <= str1.length; i++) {
        track[0][i] = i;
    }
    for (var j = 0; j <= str2.length; j++) {
        track[j][0] = j;
    }
    for (var j = 1; j <= str2.length; j++) {
        for (var i = 1; i <= str1.length; i++) {
            var indicator = str1[i - 1] === str2[j - 1] ? 0 : 1;
            track[j][i] = Math.min(
                track[j][i - 1] + 1,                // deletion
                track[j - 1][i] + 1,                // insertion
                track[j - 1][i - 1] + indicator     // substitution
            );
        }
    }
    return track[str2.length][str1.length];
};

replaceAll = function (string, prev, replacer) {
    var old = string;
    while (true) {
        new_ = old.replace(prev, replacer);
        if (new_ == old) return old;
        old = new_;
    }
};

quickDistance = function (str1, str2) {
    var str1_words = replaceAll(replaceAll(str1, "-", " "), "_", " ").split(
        " "
    );
    var str2_words = replaceAll(replaceAll(str2, "-", " "), "_", " ").split(
        " "
    );
    var leads1 = str1_words.map(function (word) {
        return word[0] || "0";
    });
    var leads2 = str2_words.map(function (word) {
        return word[0] || "0";
    });
    return levenshteinDistance(leads1, leads2);
};

binarySearch = function(haystack, needle, caseInsensitive, callback) {
	if (typeof haystack === 'undefined' || haystack.length <= 0 || typeof needle === 'undefined') {
		return -1; // what?
	}

	var len = haystack.length;
	var high = len - 1;
	var low = 0;
	var mid = 0;
	var element = '';
    var exact = false;
    var idx = mid;

	needle = caseInsensitive ? needle.toLowerCase() : needle;

	while (low <= high) {
        if(callback) callback(1 - Math.max(0, (high + 1 - low) / len));
		mid = parseInt((low + high) / 2, 10);
		element = caseInsensitive ? haystack[mid].toLowerCase() : haystack[mid];
		if (element === needle) {
            exact = true
			idx = mid;
            break;
		} else if (element < needle) {
			low = mid + 1;
		} else {
			high = mid - 1;
		}
	}
    if(!exact) idx = low;
	if(callback) callback(1);

	return {
        exact: exact,
        index: idx,
        value: haystack[idx]
    };
};

LRU = function () {
    var cache = {};
    function handle(f) {
        function wrapper() {
            if (arguments.length <= 16) {
                var cacheKey = JSON.stringify(arguments);
                if (cache[cacheKey]) return cache[cacheKey];
                var result = f(
                    arguments[0],
                    arguments[1],
                    arguments[2],
                    arguments[3],
                    arguments[4],
                    arguments[5],
                    arguments[6],
                    arguments[7],
                    arguments[8],
                    arguments[9],
                    arguments[10],
                    arguments[11],
                    arguments[12],
                    arguments[13],
                    arguments[14],
                    arguments[15]
                );
                cache[cacheKey] = result;
                return result;
            }
            throw "Cannot handle more than 16 args";
        }
        return wrapper;
    }
    function clear() {
        cache = {};
    }
    function remove(key) {
        delete cache[key];
    }

    return {
        cache: handle,
        clear: clear,
        remove: remove,
    };
};

var searchFilesLru = LRU();
listdir = searchFilesLru.cache(function (path, pattern) {
    var topFolder = new Folder(path);
    var files = topFolder.getFiles();
    return files.filter(function (f) {
        if (!pattern) return true;
        return decodeURIComponent(f.fullName).match(pattern);
    });
});

listdirNames = function (path, pattern) {
    return listdir(path, pattern).map(function (f) {
        return decodeURIComponent(f.name);
    });
};

isFolderExists = function (path) {
    return Folder(path).exists;
};

EventEmitter = function () {
    return {
        listeners: {},
        on: function (name, callback) {
            (this.listeners[name] || (this.listeners[name] = [])) &&
                this.listeners[name].push(callback);
        },
        emit: function (name, message) {
            (this.listeners[name] || []).forEach(function (v) {
                v(message);
            });
        },
        unbind: function (name) {
            this.listeners[name] && (this.listeners[name] = null);
        },
        unbindAll: function () {
            this.listeners = {};
        },
    };
};

Dialog = function (config) {
    var dialog = new Window("dialog", config.title || "-- Dialog --");
    var data = {};

    function buildLayout(cfg, parent) {
        var eid = cfg.id;
        var dispatchTable = {
            tabbedpanel: function() {
                var tabbedpanel = parent.add("tabbedpanel");
                return tabbedpanel;
            },
            tab: function() {
                var tab = parent.add("tab", undefined, cfg.title);
                return tab;
            },
            panel: function () {
                var panel = parent.add("panel", undefined, cfg.title);
                return panel;
            },
            edittext: function () {
                var edittext = parent.add("edittext", undefined, cfg.value, {multiline: !!cfg.multiline, scrolling: false, readonly: !!cfg.readonly});
                return edittext;
            },
            statictext: function() {
                var statictext = parent.add("statictext", undefined, cfg.value, {multiline: !!cfg.multiline, scrolling: false, readonly: !!cfg.readonly});
                return statictext;
            },
            button: function () {
                var button = parent.add("button", undefined, cfg.title);
                return button;
            },
            group: function () {
                var group = parent.add("group");
                return group;
            },
            progressbar: function () {
                var progressbar = parent.add("progressbar", undefined, 0, 100);
                return progressbar;
            },
            image: function() {
                var image = parent.add("image", undefined, File(cfg.location));
                return image;
            },
            checkbox: function() {
                var checkbox = parent.add("checkbox", undefined, cfg.title);
                checkbox.value = !!cfg.value;
                return checkbox;
            }
        };
        var element = dispatchTable[cfg.type] && dispatchTable[cfg.type]();
        if (parent == undefined) {
            element = dialog;
            data.root = dialog;
        }
        if (eid && eid != "root") {
            data[eid] = element;
        }
        if (cfg.box) {
            var left = cfg.box[0];
            var top = cfg.box[1];
            var width = cfg.box[2];
            var height = cfg.box[3];
            if (left != undefined || top != undefined) {
                element.margins = [left || 0, top || 0, 0, 0];
            }
            if (width != undefined || height != undefined) {
                element.size = {
                    width: width || 0,
                    height: height || 0,
                };
            }
        }
        if (cfg.orientation) element.orientation = cfg.orientation; // row, column
        if (cfg.alignChildren) element.alignChildren = cfg.alignChildren; // left, right, top, bottom, center
        if (cfg.alignment) element.alignment = cfg.alignment; // left, right, top, bottom, center
        if (cfg.body && element) {
            cfg.body.forEach(function (subcfg) {
                buildLayout(subcfg, element);
            });
        }
    }
    buildLayout(config);

    return data;
};

getSharedConfig = function() {
    if(!File(cwd + '/shared-settings/autofill-config.json').exists) return {};
    return JSON.parse(readFile(cwd + '/shared-settings/autofill-config.json'));
};

updateSharedConfig = function(config) {
    writeText(cwd + '/shared-settings/autofill-config.json', JSON.stringify(config));
};

getLocalConfig = function(name) {
    if(!name) name = '';
    if (!File(cwd + "/shared-settings/local-"+pcname+"--"+name+".json").exists) return {};
    return JSON.parse(readFile(cwd + "/shared-settings/local-"+pcname+"--"+name+".json"));
};

updateLocalConfig = function(config, name) {
    if(!name) name = '';
    try {
        writeText(cwd + "/shared-settings/local-"+pcname+"--"+name+".json", JSON.stringify(config));
    } catch (e) {
        alert(e);
    }
};

exportPSDtoPNG = function(psdLocation, pngLocation) {
    app.open(File(psdLocation));
    activeDocument.saveAs(File(pngLocation), pngOpts, true);
    activeDocument.close(SaveOptions.DONOTSAVECHANGES);
};

cmd = function(command) {
    var stdout = '';
    var tempFile = File(cwd + "/command-zone/"+uuidv4()+'.txt');
    var batFile = File(cwd + "/command-zone/"+uuidv4()+'.bat');
    batFile.open('w');
    batFile.write('echo off\nchcp 65001\n' + command + " > \"" + tempFile.fsName+'"');
    batFile.close();
    batFile.execute();
    $.sleep(300);
    batFile.remove();
    tempFile = File(tempFile.fsName);
    if (tempFile.open("r")) {
        stdout = tempFile.read();
        tempFile.close();
        tempFile.remove();
    }
    return stdout;
};

