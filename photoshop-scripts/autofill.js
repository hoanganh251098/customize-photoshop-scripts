const doc = activeDocument;


// image shrink/scale to fit
Image.prototype.onDraw = function () {
    // written by Marc Autret
    // "this" is the container; "this.image" is the graphic
    if (!this.image) return;
    var WH = this.size,
        wh = this.image.size,
        k = Math.min(WH[0] / wh[0], WH[1] / wh[1]),
        xy;
    // Resize proportionally:
    wh = [k * wh[0], k * wh[1]];
    // Center:
    xy = [(WH[0] - wh[0]) / 2, (WH[1] - wh[1]) / 2];
    this.graphics.drawImage(this.image, xy[0], xy[1], wh[0], wh[1]);
    WH = wh = xy = null;
};

$.evalFile(new File($.fileName).parent + "/import.js");
clearLog();

function getUserName() {
    var isMacOS = typeof isMacOS != 'undefined' && isMacOS();
    return isMacOS ? $.getenv("USER") : $.getenv("USERNAME");
}

const pcname = getUserName();

function debugVar(v) {
    $.level = 2;
    debugger;
}


function parseDateFromSKU(sku) {
    try {
        var results = sku.match(/\d{6}/g)
        var yy = results[0].substring(4,6)
        var mm = results[0].substring(2,4)
        var dd = results[0].substring(0,2)
        return {
            yy: yy,
            mm: mm,
            dd: dd
        };
    }
    catch(e) {
        return null;
    }
}


var data = {
    psdSearchFolders: ["\\\\192.168.1.168\\temp", "\\\\192.168.1.111\\temp2"],
    outputFolders: [],
};

function renderProgress(percent) {
    dialog["progress"].value = percent;
    dialog.root.update();
}
function flashProgress() {
    var progressBar = dialog["progress"];
    for (var i = 0; i <= 100; i++) {
        renderProgress(i);
    }
    renderProgress(0);
}
function smoothProgress(target) {
    var currentProgress = dialog["progress"].value;
    var step = (target - currentProgress) / 100;
    for (var i = 0; i <= 100; i++) {
        currentProgress = currentProgress + step;
        renderProgress(currentProgress);
    }
    renderProgress(target);
}

function openPSD(path) {
    var f = File(path);
    exeLog("Opening " + f.fsName);
    var openedDoc = app.open(f);
    exeLog(f.fsName + " opened.");
    return openedDoc;
}

var exeLogArray = [];
function exeLog(text) {
    var linesLimit = 15;
    if(exeLogArray.length && exeLogArray.slice(-1)[0].text == text) {
        exeLogArray.slice(-1)[0].count++;
    }
    else {
        exeLogArray.push({
            text: text,
            count: 1
        });
    }
    var output = '';
    exeLogArray = exeLogArray.slice(-linesLimit);
    exeLogArray.forEach(function(l) {
        if(l.count > 1) output += '['+l.count+'] ';
        output += l.text + '\n';
    });
    var lines = output.split('\n');
    var limitedLines = [];
    lines.forEach(function(line) {
        (line.match(/.{1,90}/g) || []).forEach(function(l) {
            limitedLines.push(l);
        });
    });
    dialog["log"].text = limitedLines.slice(-linesLimit).join('\n');
    dialog.root.update();
}
function clearExeLog() {
    exeLogArray = [];
    dialog["log"].text = "";
    dialog.root.update();
}
function checkOutputDuplicated(outputSKU) {}
function openPSDFolder(path) {}

var listed = [];

function loadListed(idx) {
    if (!listed[idx]) {
        var folder = data.psdSearchFolders[idx];
        var content = readFile(
            cwd + "/command-zone/listed-temp-" + idx + ".txt"
        ).split("\n");
        var sharedConfig = getSharedConfig();
        if (!sharedConfig.listed) sharedConfig.listed = [];
        if (!sharedConfig.listed[idx]) sharedConfig.listed[idx] = {};
        if (!sharedConfig.listed[idx].sorted) {
            exeLog("Sorting " + folder + " ...");
            content.sort();
            writeText(
                cwd + "/command-zone/listed-temp-" + idx + ".txt",
                content.join("\n")
            );
            exeLog("Done");
            sharedConfig.listed[idx].sorted = true;
        }
        updateSharedConfig(sharedConfig);
        listed[idx] = content;
    }
    return listed[idx];
}

var dialog = Dialog({
    title: "Customize Autofill",
    body: [
        {
            type: "tabbedpanel",
            id: "tabs",
            body: [
                {
                    type: "tab",
                    title: "Input",
                    box: [0, 0, 500, 480],
                    body: [
                        {
                            type: "group",
                            orientation: "column",
                            box: [15, 5],
                            body: [
                                { type: "group", box: [0, 0, 500, 0] }, // stretcher
                                {
                                    type: "edittext",
                                    id: "custom-text",
                                    box: [0, 0, 500, 435],
                                    value: "",
                                    multiline: true,
                                },
                                {
                                    type: "group",
                                    alignment: "right",
                                    body: [
                                        {
                                            type: "button",
                                            id: "reindex",
                                            title: "Index search folders",
                                        },
                                        {
                                            type: "button",
                                            id: "process-queue",
                                            title: "Process",
                                        },
                                        {
                                            type: "button",
                                            id: "enqueue",
                                            title: "Input to process",
                                        },
                                    ],
                                },
                            ],
                        },
                    ],
                },
                {
                    type: "tab",
                    title: "Output",
                    box: [0, 0, 500, 480],
                    body: [
                        {
                            type: "group",
                            orientation: "column",
                            box: [15, 5],
                            body: [
                                { type: "group", box: [0, 0, 500, 0] }, // stretcher
                                {
                                    type: "group",
                                    alignment: "right",
                                    body: [
                                        {
                                            type: "checkbox",
                                            id: "ok-checkbox",
                                            title: "OK"
                                        },
                                        {
                                            type: "button",
                                            id: "edit-psd",
                                            title: "Edit PSD",
                                        },
                                        {
                                            type: "button",
                                            id: "prev-image",
                                            title: "<< Prev",
                                        },
                                        {
                                            type: "button",
                                            id: "next-image",
                                            title: "Next >>",
                                        },
                                    ],
                                },
                            ],
                        },
                        {
                            type: "group",
                            orientation: "column",
                            box: [15, 15],
                            body: [
                                {
                                    type: "panel",
                                    id: 'preview-image-wrapper',
                                    title: "Preview",
                                    body: [
                                        {
                                            type: "image",
                                            id: "preview-image",
                                            box: [15, 0, 465, 400],
                                            location: cwd + "/export/demo.png",
                                        },
                                    ],
                                },
                            ],
                        },
                    ],
                },
                {
                    type: "tab",
                    title: "Settings",
                    box: [0, 0, 500, 300],
                    body: [
                        {
                            type: "group",
                            orientation: "column",
                            box: [15, 15],
                            body: [
                                {
                                    type: "panel",
                                    title: "PSD search folders",
                                    body: [
                                        {
                                            type: "edittext",
                                            id: "search-folders",
                                            box: [15, 0, 465, 100],
                                            value: "",
                                            multiline: true,
                                        },
                                    ],
                                },
                                {
                                    type: "panel",
                                    title: "Output folder",
                                    body: [
                                        {
                                            type: "edittext",
                                            id: 'output-folder',
                                            box: [15, 0, 465, 0],
                                            value: "",
                                        },
                                    ],
                                },
                                {
                                    type: "group",
                                    box: [0, 20, 465, 50],
                                    body: [
                                        {
                                            type: "button",
                                            id: "save-settings",
                                            box: [0, 0, 465, 0],
                                            title: "Save",
                                        },
                                    ],
                                },
                            ],
                        },
                    ],
                },
                {
                    type: "tab",
                    title: "DEV",
                    box: [0, 0, 500, 480],
                    body: [
                        {
                            type: "group",
                            orientation: "column",
                            box: [15, 5],
                            body: [
                                { type: "group", box: [0, 0, 500, 0] }, // stretcher
                                {
                                    type: "group",
                                    alignment: "left",
                                    body: [
                                        {
                                            type: "button",
                                            id: "test",
                                            title: "TEST",
                                        },
                                        {
                                            type: "button",
                                            id: "flash",
                                            title: "Test Progress Bar",
                                        },
                                        {
                                            type: "button",
                                            id: "find-texts",
                                            title: "Find text layers",
                                        },
                                        {
                                            type: "button",
                                            id: "search-sku",
                                            title: "Search SKU",
                                        },
                                    ],
                                },
                            ],
                        },
                    ],
                },
            ],
        },
        {
            type: "panel",
            title: "Execution Log",
            body: [
                {
                    type: "progressbar",
                    id: "progress",
                    box: [undefined, undefined, 500, 10],
                },
                {
                    type: "statictext",
                    id: "log",
                    box: [0, 0, 500, 200],
                    value: "",
                    multiline: true,
                },
            ],
        },
    ],
});

function restoreTab() {
    dialog["tabs"].selection = (function () {
        var localConfig = getLocalConfig();
        if (localConfig.openingTab == undefined) {
            localConfig.openingTab = 0;
            updateLocalConfig(localConfig);
        }
        return localConfig.openingTab;
    })();
}
restoreTab();

(function loadRawInput() {
    var localConfig = getLocalConfig();
    if(localConfig.rawInput) dialog['custom-text'].text = localConfig.rawInput;
})();

(function loadSettings() {
    var localConfig = getLocalConfig();
    var outputFolder = localConfig.outputFolder || '';
    dialog['output-folder'].text = outputFolder;
})();

function saveCurrentTab() {
    var localConfig = getLocalConfig();
    var selectedTabIndex = null;
    toArray(dialog["tabs"].children).forEach(function (tab, idx) {
        var selectedTab = dialog["tabs"].selection;
        if (selectedTab == tab) {
            selectedTabIndex = idx;
        }
    });
    localConfig.openingTab = selectedTabIndex;
    updateLocalConfig(localConfig);
}

var showingPreview = {
    imageIdx: 0
};
function listPreviewInfo() {
    var localConfig = getLocalConfig();
    var result = [];
    Object.entries(localConfig.request).forEach(function(skuEntry) {
        var sku = skuEntry[0];
        skuEntry[1].customData.forEach(function(customInfo) {
            var customText = customInfo.text;
            var imageList = listdirNames(customInfo.output, /\.png/g);
            imageList.forEach(function(image) {
                result.push({
                    sku: sku,
                    searchResult: skuEntry[1].searchResult,
                    customText: customText,
                    image: customInfo.output + '\\' + image,
                    isOK: customInfo.isOK,
                });
            });
        });
    });
    return result;
}
function showPreviewing() {
    try {
        var previewInfo = listPreviewInfo()[showingPreview.imageIdx];
        dialog['preview-image'].image = previewInfo.image;
        dialog['ok-checkbox'].value = previewInfo.isOK;
        exeLog('-------------------------------')
        exeLog('Showing SKU: ' + previewInfo.sku);
        exeLog('Custom text: ' + previewInfo.customText);
        exeLog('Showing image: ' + previewInfo.image);
        return true;
    }
    catch(e) {
        return false;
    }
}
function refreshPreviewImages() {
    var localConfig = getLocalConfig();
    showingPreview.imageIdx = localConfig.showingImage || 0;
    if(!showPreviewing()) {
        showingPreview.imageIdx = 0;
        showPreviewing();
    }
}
refreshPreviewImages();

dialog['prev-image'].onClick = function() {
    showingPreview.imageIdx -= 1;
    var previewInfo = listPreviewInfo()[showingPreview.imageIdx];
    if(!previewInfo) {
        showingPreview.imageIdx += 1;
        return;
    }
    showPreviewing();
};

dialog['next-image'].onClick = function() {
    showingPreview.imageIdx += 1;
    var previewInfo = listPreviewInfo()[showingPreview.imageIdx];
    if(!previewInfo) {
        showingPreview.imageIdx -= 1;
        return;
    }
    showPreviewing();
};

dialog["tabs"].onChange = function () {
    saveCurrentTab();
};

dialog["search-folders"].text = data.psdSearchFolders.join("\n") + "\n";

function searchSKU(sku) {
    try {
        var containingFolder;
        var bestMatch;
        var found = false;
        log("Searching for sku: " + sku);

        for (var idx = 0; idx < data.psdSearchFolders.length; idx++) {
            var folder = data.psdSearchFolders[idx];
            var content = loadListed(idx);
            var lastLogPercent = 0;
            containingFolder = folder;
            var bestMatch = binarySearch(
                content,
                sku,
                true,
                function (percent) {
                    if (percent - lastLogPercent > 0.1) {
                        lastLogPercent += 0.1;
                        exeLog(
                            "Searching " +
                                folder +
                                " ... " +
                                lastLogPercent * 100 +
                                "%"
                        );
                        log(
                            "Searching " +
                                folder +
                                " ... " +
                                lastLogPercent * 100 +
                                "%"
                        );
                    }
                }
            ).value;
            if (bestMatch && bestMatch.toLowerCase().indexOf(sku.toLowerCase()) != -1) {
                found = true;
                break;
            }
        }
        log("Best match: " + bestMatch);
        if (!found) {
            exeLog("Not found!");
            return null;
        }
        exeLog("Found " + containingFolder + "\\" + bestMatch);
        return containingFolder + "\\" + bestMatch;
    } catch (e) {
        alert(e);
        logjson(e);
    }
}

dialog["reindex"].onClick = function () {
    var sharedConfig = getSharedConfig();
    sharedConfig.listed = [];
    updateSharedConfig(sharedConfig);
    exeLog("Indexing ...");
    data.psdSearchFolders.forEach(function (folder, index) {
        writeText(cwd + "/command-zone/listed-temp-" + index + ".txt", "");
        var destFile = File(
            cwd + "/command-zone/listed-temp-" + index + ".txt"
        );
        writeText(
            cwd + "/command-zone/list-temp-" + index + ".bat",
            "echo off\nchcp 65001\ndir " +
                folder +
                "  /b /ON > " +
                destFile.fsName
        );
        File(cwd + "/command-zone/list-temp-" + index + ".bat").execute();
    });
    $.sleep(2000);
    for (var idx = 0; idx < data.psdSearchFolders.length; idx++) {
        loadListed(idx);
    }
    flashProgress();
};

dialog["enqueue"].onClick = function () {
    try {
        var localConfig = getLocalConfig();
        var text = dialog["custom-text"].text;
        localConfig.rawInput = text;
        updateLocalConfig(localConfig);
        var lines = text.split("\n");
        var skuGroups = {};
        var nrErrors = 0;
        var nrMissing = 0;
        lines.forEach(function (line) {
            var sku = line.split("\t")[0];
            var customText = line.split("\t")[1];
            if (!customText) {
                nrErrors += 1;
                return;
            }
            if (!skuGroups[sku]) {
                skuGroups[sku] = {
                    searchResult: searchSKU(sku),
                    customData: [],
                    isFile: false,
                    parsedDate: parseDateFromSKU(sku),
                };
                var pdata = skuGroups[sku];
                if (pdata.searchResult == null) {
                    nrMissing += 1;
                } else {
                    if (
                        pdata.searchResult.toLowerCase().indexOf(".psd") != -1
                    ) {
                        pdata.isFile = true;
                    }
                }
            }
            skuGroups[sku].customData.push({
                text: customText,
                mergedSKU:
                    sku +
                    "_" +
                    encodeURIComponent(replaceAll(customText, " ", "_")),
                output: "",
                isOK: false,
            });
        });
        var localConfig = getLocalConfig();
        localConfig.request = skuGroups;
        updateLocalConfig(localConfig);
        logjson(localConfig);
        clearExeLog();
        exeLog(
            "Done. Error: " +
                nrErrors +
                ", missing: " +
                nrMissing +
                ", total: " +
                Object.entries(skuGroups).length +
                "."
        );
        exeLog("Click 'Process' to start exporting.");
    } catch (e) {
        logjson(e);
    }
};

function setAllTextValue(value) {
    query("text").forEach(function (layer) {
        try {
            if(!layer.allLocked) {
                layer.textItem.contents = value;
            }
        }
        catch(e) {
            exeLog(''+e);
            logjson(e);
        }
    });
}

dialog["process-queue"].onClick = function () {
    var localConfig = getLocalConfig();
    if(!localConfig.outputFolder) {
        alert('Output folder not found!');
        return;
    }
    var progressTotal = 0;
    var progressCurrent = 0;
    Object.entries(localConfig.request).forEach(function(skuGroup) {
        var skuInfo = skuGroup[1];
        progressTotal += skuInfo.customData.length;
    });
    Object.entries(localConfig.request).forEach(function (skuGroup) {
        try {
            var sku = skuGroup[0];
            var skuInfo = skuGroup[1];
            exeLog('Input folder: ' + skuInfo.searchResult);
            var openedFiles = [];
            if(skuInfo.isFile) {
                openedFiles = [{
                    name: 'untitled',
                    document: openPSD(skuInfo.searchResult)
                }];
            }
            else if(skuInfo.searchResult) {
                var psdFiles = listdir(skuInfo.searchResult, /\.psd/g);
                psdFiles.forEach(function(psdFile) {
                    var f = openPSD(psdFile.fsName);
                    openedFiles.push({
                        name: f.name,
                        document: f
                    });
                });
            }
            skuInfo.customData.forEach(function (data) {
                var parsedDate = skuInfo.parsedDate;
                var subFolderPath = '20'+parsedDate.yy+'/Thang '+parsedDate.mm+'-'+parsedDate.yy+'/'+parsedDate.yy+parsedDate.mm+parsedDate.dd;
                var outputFolder = Folder(localConfig.outputFolder + '/' + subFolderPath + '/' + data.mergedSKU);
                if(!outputFolder.exists) {
                    outputFolder.create();
                    exeLog('Creating folder: ' + outputFolder.fsName);
                }
                if(!data.isOK) {
                    openedFiles.forEach(function(psdDoc) {
                        activeDocument = psdDoc.document;
                        exeLog('Writing text: ' + data.text);
                        setAllTextValue(data.text);
                    });
                    openedFiles.forEach(function(psdDoc) {
                        activeDocument = psdDoc.document;
                        var exportPath = outputFolder.fsName + '/' + replaceAll(psdDoc.name, '.psd', '') + ".png";
                        exeLog('Exporting to ' + exportPath);
                        activeDocument.saveAs(File(exportPath), pngOpts, true);
                        exeLog('Exported.');
                    });
                    data.output = outputFolder.fsName;
                    updateLocalConfig(localConfig);
                }
                progressCurrent += 1;
                smoothProgress((progressCurrent / progressTotal) * 100);
            });
            openedFiles.forEach(function(psdDoc) {
                activeDocument = psdDoc.document;
                activeDocument.close(SaveOptions.DONOTSAVECHANGES);
            });
        }
        catch(e) {
            alert(e);
            logjson(e);
        }
    });
    updateLocalConfig(localConfig);
    smoothProgress(0);
    exeLog("Process done.");
    alert('Done!');
};

dialog['edit-psd'].onClick = function() {
    var previewing = listPreviewInfo()[showingPreview.imageIdx];
    var isSearchResultFile = previewing.searchResult.indexOf('.psd') != -1;
    if(isSearchResultFile) {
        openPSD(previewing.searchResult);
    }
    else {
        var psdFiles = listdir(previewing.searchResult, /\.psd/g);
        psdFiles.forEach(function(psdFile) {
            openPSD(psdFile.fsName)
        });
    }
    dialog.root.close();
};

dialog['ok-checkbox'].onClick = function() {
    var newValue = dialog['ok-checkbox'].value;
    var previewing = listPreviewInfo()[showingPreview.imageIdx];
    var localConfig = getLocalConfig();
    for(var sku in localConfig.request) {
        if(sku != previewing.sku) continue;
        for(var i in localConfig.request[sku].customData) {
            var customInfo = localConfig.request[sku].customData[i];
            if(customInfo.text == previewing.customText) {
                customInfo.isOK = newValue;
            }
        }
    }
    updateLocalConfig(localConfig);
};

dialog['save-settings'].onClick = function() {
    var outputFolder = Folder(dialog['output-folder'].text || '');
    if(!outputFolder.exists) {
        alert('Output folder does not exists!');
        return;
    }
    var localConfig = getLocalConfig();
    localConfig.outputFolder = outputFolder.fsName;
    updateLocalConfig(localConfig);
    alert('Saved.');
};

dialog["flash"].onClick = function () {
    smoothProgress(100);
    smoothProgress(0);
};

dialog["test"].onClick = function () {
    try {
        // exeLog(JSON.stringify(parseDateFromSKU('jd13-weed-090920-custom'), undefined, 2));
    } catch (e) {
        logjson(e);
    }
};

dialog["find-texts"].onClick = function () {
    try {
        var textLayers = query("text");
        alert(textLayers.length + ' ' + JSON.stringify(textLayers.map(function(layer) {
            return layer.name;
        }), undefined, 2));
    }
    catch(e) {
        alert(e);
        logjson(e);
    }
};

dialog["search-sku"].onClick = function () {
    try {
        var sku = prompt('SKU');
        prompt('', searchSKU(sku));
    }
    catch(e) {
        alert(e);
        logjson(e);
    }
};

dialog.root.onClose = function() {
    var localConfig = getLocalConfig();
    localConfig.showingImage = showingPreview.imageIdx;
    updateLocalConfig(localConfig);
}

dialog.root.show();
