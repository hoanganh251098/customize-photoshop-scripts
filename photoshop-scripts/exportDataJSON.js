var env = 'prod';
// env = 'dev';

const help = 
'    - Generate skeleton: Tạo nhanh cấu trúc các layer (chỉ bấm được khi file psd là file mới).\n\n' + 
'    - Save backgrounds and textures: Lưu các background và texture thành png trong folder images.\n\n' + 
'    - Save markers: Lưu các map marker thành png trong folder images (yêu cầu marker phải là Smart Object).\n\n' + 
'    - Process custom bg colors: Đổi tên các layer trong group "backgroundColors" thành mã màu.\n\n' + 
'    - Export data.json: Xuất ra file data.json.\n\n' + 
'    ** Nhớ tổ chức các layer đúng tên với cấu trúc nhé, nếu không sẽ bị lỗi! **\n\n' + 
'Cần gì thêm thì hỏi Hoàng Anh nhé!';
const faq = 
'    Q: Tại sao lại nhiều nút vậy, sao không gộp hết lại vào nút "Export data.json"?\n' + 
'    A: Các nút chức năng được tách ra dựa trên thời gian thực thi (cái nào chạy chậm thì bị tách ra thành nút riêng).\n\n' + 
'    Q: Tại sao lên web lại bị lỗi font?\n' + 
'    A: Check lại path của font trong data.json xem đúng chưa nhé!\n\n' + 
'    Q: Tại sao upload photo rồi mà vẫn bị đen?\n' + 
'    A: Chắc do ảnh nặng quá nên trình duyệt load lâu\nhoặc là lỗi gì đó...\n\n' + 
'    Q: Sao bấm "Save backgrounds and textures" lại hiện lên cửa sổ save as?\n' + 
'    A: Do file ko save được dưới dạng png (có thể do dùng hệ màu cmyk).\n\n' + 
'    Q: Sao lên web không load background color đầu tiên?\n' + 
'    A: Web mặc định load background image đầu tiên nên kéo cái color đó lên trên cùng của group background images là được.\n\n' + 
'    Q: Tắt cái cursor ở giữa màn hình?\n' + 
'    A: Ctrl + H một lần ko được thì nhiều lần nhé. Ko thì vào Menu->View->Extras rồi tắt đi.\n\n' + 
'    Q: Map không có marker có được không?\n' + 
'    A: Kéo opacity của marker về 0 là được.\n\n' + 
'Tạm thời thế...';


const doc = activeDocument;
const cwd = (new File($.fileName)).parent;
function loadScript(path) {
    try { // relative path
        $.evalFile(cwd + '/' + path);
    } catch (e) { // absolute path
        $.evalFile(path);
    }
}

loadScript('import/json2.js');
loadScript('import/clean.jsx');
loadScript('import/save_options.js');

function writeJSON(path, data) {
    try {
        const f = new File(path);
        f.open('w');
        f.writeln(JSON.stringify(data, null, 4));
        f.close();
    } catch (e) {
        alert('Error: ' + e.message);
    }
}

function loadJSON(path) {
    try {
        const f = new File(path);
        f.open('r');
        const data = JSON.parse(f.read());
        f.close();
        return data;
    } catch (e) {
        alert('Error: ' + e.message);
    }
}

function writeText(path, data) {
    try {
        const f = new File(path);
        f.open('w');
        f.writeln(data);
        f.close();
    } catch (e) {
        alert('Error: ' + e.message);
    }
}

const box = new Window('dialog', "Export JSON");

var helpBtn = box.add('button', undefined, "Help", undefined);
helpBtn.alignment = ["left", "top"];
var faqBtn = box.add('button', undefined, "FAQ", undefined);
faqBtn.alignment = ["left", "top"];
var generateSkeletonBtn = box.add('button', undefined, "Generate skeleton", undefined);
generateSkeletonBtn.alignment = ["left", "top"];
var saveBackgroundsBtn = box.add('button', undefined, "Save backgrounds and textures", undefined);
saveBackgroundsBtn.alignment = ["left", "top"];
var saveMarkersBtn = box.add('button', undefined, "Save markers", undefined);
saveMarkersBtn.alignment = ["left", "top"];
var processCustomBgColorsBtn = box.add('button', undefined, "Process custom bg colors", undefined);
processCustomBgColorsBtn.alignment = ["left", "top"];
var exportDataBtn = box.add('button', undefined, "Export data.json", undefined);
exportDataBtn.alignment = ["left", "top"];
if (env == 'dev') {
    var hideAllBtn = box.add('button', undefined, "Hide all (recursive)", undefined);
    hideAllBtn.alignment = ["left", "top"];
    var showAllBtn = box.add('button', undefined, "Show all (recursive)", undefined);
    showAllBtn.alignment = ["left", "top"];
    var testBtn = box.add('button', undefined, "Test", undefined);
    testBtn.alignment = ["left", "top"];
    testBtn.onClick = test;
    
    hideAllBtn.onClick = function () {
        helper.hideAll();
        alert('OKKK!');
    };
    showAllBtn.onClick = function () {
        helper.showAll();
        alert('OKKK!');
    };
}

var okButton = box.add("button", undefined, undefined, {name: "okButton"});
okButton.text = "OK";
okButton.alignment = ["right", "bottom"];
okButton.graphics.font = "dialog:13";

helpBtn.addEventListener('click', function (k) {
    var alertWindow = new Window("dialog", undefined, undefined, {resizeable: false});
    alertWindow.text = 'Bạn cần giúp đỡ?'

    var alertText = alertWindow.add("group");
    alertText.orientation = "column";
    alertText.alignChildren = ["left", "center"];
    alertText.add("statictext", undefined, help, {name: "alertText", multiline: true});

    var okButton = alertWindow.add("button", undefined, undefined, {name: "okButton"});
    okButton.text = "OK";
    okButton.alignment = ["right", "bottom"];
    okButton.graphics.font = "dialog:13";

    alertWindow.show();
});
faqBtn.addEventListener('click', function (k) {
    var alertWindow = new Window("dialog", undefined, undefined, {resizeable: false});
    alertWindow.text = 'Những câu hay bị hỏi'

    var alertText = alertWindow.add("group");
    alertText.orientation = "column";
    alertText.alignChildren = ["left", "center"];
    alertText.add("statictext", undefined, faq, {name: "alertText", multiline: true});

    var okButton = alertWindow.add("button", undefined, undefined, {name: "okButton"});
    okButton.text = "OK";
    okButton.alignment = ["right", "bottom"];
    okButton.graphics.font = "dialog:13";

    alertWindow.show();
});
generateSkeletonBtn.onClick = generateSkeleton;
saveBackgroundsBtn.onClick = saveBackgrounds;
saveMarkersBtn.onClick = saveMarkers;
processCustomBgColorsBtn.onClick = processCustomBgColors;
exportDataBtn.onClick = exportData;

const components = {
    bgColors: [],
    bgImages: [],
    texts: [],
    masks: [],
    maps: [],
    songs: []
};

const helper = {
    getLayerDimensionsInfo: function (layer) {
        try {
            var width = layer.width.value;
            var height = layer.height.value;
        } catch (e) {
            var width = layer.bounds[2].value - layer.bounds[0].value;
            var height = layer.bounds[3].value - layer.bounds[1].value;
        }
        return {
            left: layer.bounds[0].value,
            top: layer.bounds[1].value,
            width: width,
            height: height,
            rotate: 0,
            opacity: layer.opacity / 100,
            visible: true,
            blendMode: 'normal'
        };
    },
    getLayer: function (parentLayer, childLayerName) {
        try {
            return parentLayer.getByName(childLayerName);
        } catch (e) {
            alert("No layer named '" + childLayerName + "'");
            return null;
        }
    },
    backupVisibilityState: function () {
        const state = {};
        function dfs(layer, path) {
            const sublayers = layer.layers || layer.layerSets;
            if (sublayers && sublayers.length) {
                for (var i = 0; i < sublayers.length; i++) {
                    state[path + i] = sublayers[i].visible;
                    dfs(sublayers[i], path + i);
                }
            }
        }
        dfs(activeDocument, '');
        return state;
    },
    restoreVisibilityState: function (state) {
        function dfs(layer, path) {
            const sublayers = layer.layers || layer.layerSets;
            if (sublayers && sublayers.length) {
                for (var i = 0; i < sublayers.length; i++) {
                    sublayers[i].visible = state[path + i];
                    dfs(sublayers[i], path + i);
                }
            }
        }
        dfs(activeDocument, '');
    },
    hideAll: function (l) {
        function dfs(layer) {
            const sublayers = layer.layers || layer.layerSets;
            if (sublayers && sublayers.length) {
                for (var i = 0; i < sublayers.length; i++) {
                    sublayers[i].visible = false;
                    dfs(sublayers[i]);
                }
            }
        }
        dfs(l ? l : activeDocument);
    },
    showAll: function (l) {
        function dfs(layer) {
            const sublayers = layer.layers || layer.layerSets;
            if (sublayers && sublayers.length) {
                for (var i = 0; i < sublayers.length; i++) {
                    sublayers[i].visible = true;
                    dfs(sublayers[i]);
                }
            }
        }
        dfs(l ? l : activeDocument);
    },
    getActiveLayerSVG: function () {
        var layer = activeDocument.activeLayer;
        var layerinfo = helper.getLayerDimensionsInfo(layer);
        var idcopyLayerSVG = stringIDToTypeID("copyLayerSVG");
        var actionDescriptor = new ActionDescriptor();
        var idnull = charIDToTypeID("null");
        var actionReference = new ActionReference();
        var idLyr = charIDToTypeID("Lyr ");
        var idOrdn = charIDToTypeID("Ordn");
        var idTrgt = charIDToTypeID("Trgt");
        actionReference.putEnumerated(idLyr, idOrdn, idTrgt);
        actionDescriptor.putReference(idnull, actionReference);
        executeAction(idcopyLayerSVG, actionDescriptor, DialogModes.NO);
        var r = new ActionReference();
        r.putProperty(stringIDToTypeID("property"), stringIDToTypeID("layerSVGdata"));
        r.putIdentifier(stringIDToTypeID("layer"), layer.id);
        var path = executeActionGet(r).getString(stringIDToTypeID("layerSVGdata"));
        var svg = "<svg " + 'xmlns="http://www.w3.org/2000/svg" ' + 'xmlns:xlink="http://www.w3.org/1999/xlink" ' + 'width="' + layerinfo.width + 'px" height="' + layerinfo.height + 'px">"\n';
        svg += path + '\n';
        svg += '</svg>';
        return svg;
    },
    openActiveLayer: function () {
        var idplacedLayerEditContents = stringIDToTypeID("placedLayerEditContents");
        var desc946 = new ActionDescriptor();
        executeAction(idplacedLayerEditContents, desc946, DialogModes.NO);
    },
    colorSampler: function (x, y) {
        try {
            doc.colorSamplers.removeAll();
            var curColor = doc.colorSamplers.add([x, y]);
            return curColor.color.rgb.hexValue.toLowerCase();
        } catch (e) {
            return 'ffffff'
        }
    },
    drawSomeShape: function () {
        // =======================================================
        var idslct = charIDToTypeID("slct");
        var desc934 = new ActionDescriptor();
        var idnull = charIDToTypeID("null");
        var ref229 = new ActionReference();
        var idrectangleTool = stringIDToTypeID("rectangleTool");
        ref229.putClass(idrectangleTool);
        desc934.putReference(idnull, ref229);
        var iddontRecord = stringIDToTypeID("dontRecord");
        desc934.putBoolean(iddontRecord, true);
        var idforceNotify = stringIDToTypeID("forceNotify");
        desc934.putBoolean(idforceNotify, true);
        executeAction(idslct, desc934, DialogModes.NO);

        // =======================================================
        var idMk = charIDToTypeID("Mk  ");
        var desc935 = new ActionDescriptor();
        var idnull = charIDToTypeID("null");
        var ref230 = new ActionReference();
        var idcontentLayer = stringIDToTypeID("contentLayer");
        ref230.putClass(idcontentLayer);
        desc935.putReference(idnull, ref230);
        var idUsng = charIDToTypeID("Usng");
        var desc936 = new ActionDescriptor();
        var idType = charIDToTypeID("Type");
        var desc937 = new ActionDescriptor();
        var idClr = charIDToTypeID("Clr ");
        var desc938 = new ActionDescriptor();
        var idRd = charIDToTypeID("Rd  ");
        desc938.putDouble(idRd, 34.634241);
        var idGrn = charIDToTypeID("Grn ");
        desc938.putDouble(idGrn, 92.000002);
        var idBl = charIDToTypeID("Bl  ");
        desc938.putDouble(idBl, 44.081712);
        var idRGBC = charIDToTypeID("RGBC");
        desc937.putObject(idClr, idRGBC, desc938);
        var idsolidColorLayer = stringIDToTypeID("solidColorLayer");
        desc936.putObject(idType, idsolidColorLayer, desc937);
        var idShp = charIDToTypeID("Shp ");
        var desc939 = new ActionDescriptor();
        var idunitValueQuadVersion = stringIDToTypeID("unitValueQuadVersion");
        desc939.putInteger(idunitValueQuadVersion, 1);
        var idTop = charIDToTypeID("Top ");
        var idPxl = charIDToTypeID("#Pxl");
        desc939.putUnitDouble(idTop, idPxl, 424.000000);
        var idLeft = charIDToTypeID("Left");
        var idPxl = charIDToTypeID("#Pxl");
        desc939.putUnitDouble(idLeft, idPxl, 530.000000);
        var idBtom = charIDToTypeID("Btom");
        var idPxl = charIDToTypeID("#Pxl");
        desc939.putUnitDouble(idBtom, idPxl, 952.000000);
        var idRght = charIDToTypeID("Rght");
        var idPxl = charIDToTypeID("#Pxl");
        desc939.putUnitDouble(idRght, idPxl, 1394.000000);
        var idtopRight = stringIDToTypeID("topRight");
        var idPxl = charIDToTypeID("#Pxl");
        desc939.putUnitDouble(idtopRight, idPxl, 0.000000);
        var idtopLeft = stringIDToTypeID("topLeft");
        var idPxl = charIDToTypeID("#Pxl");
        desc939.putUnitDouble(idtopLeft, idPxl, 0.000000);
        var idbottomLeft = stringIDToTypeID("bottomLeft");
        var idPxl = charIDToTypeID("#Pxl");
        desc939.putUnitDouble(idbottomLeft, idPxl, 0.000000);
        var idbottomRight = stringIDToTypeID("bottomRight");
        var idPxl = charIDToTypeID("#Pxl");
        desc939.putUnitDouble(idbottomRight, idPxl, 0.000000);
        var idRctn = charIDToTypeID("Rctn");
        desc936.putObject(idShp, idRctn, desc939);
        var idstrokeStyle = stringIDToTypeID("strokeStyle");
        var desc940 = new ActionDescriptor();
        var idstrokeStyleVersion = stringIDToTypeID("strokeStyleVersion");
        desc940.putInteger(idstrokeStyleVersion, 2);
        var idstrokeEnabled = stringIDToTypeID("strokeEnabled");
        desc940.putBoolean(idstrokeEnabled, false);
        var idfillEnabled = stringIDToTypeID("fillEnabled");
        desc940.putBoolean(idfillEnabled, true);
        var idstrokeStyleLineWidth = stringIDToTypeID("strokeStyleLineWidth");
        var idPxl = charIDToTypeID("#Pxl");
        desc940.putUnitDouble(idstrokeStyleLineWidth, idPxl, 1.000000);
        var idstrokeStyleLineDashOffset = stringIDToTypeID("strokeStyleLineDashOffset");
        var idPnt = charIDToTypeID("#Pnt");
        desc940.putUnitDouble(idstrokeStyleLineDashOffset, idPnt, 0.000000);
        var idstrokeStyleMiterLimit = stringIDToTypeID("strokeStyleMiterLimit");
        desc940.putDouble(idstrokeStyleMiterLimit, 100.000000);
        var idstrokeStyleLineCapType = stringIDToTypeID("strokeStyleLineCapType");
        var idstrokeStyleLineCapType = stringIDToTypeID("strokeStyleLineCapType");
        var idstrokeStyleButtCap = stringIDToTypeID("strokeStyleButtCap");
        desc940.putEnumerated(idstrokeStyleLineCapType, idstrokeStyleLineCapType, idstrokeStyleButtCap);
        var idstrokeStyleLineJoinType = stringIDToTypeID("strokeStyleLineJoinType");
        var idstrokeStyleLineJoinType = stringIDToTypeID("strokeStyleLineJoinType");
        var idstrokeStyleMiterJoin = stringIDToTypeID("strokeStyleMiterJoin");
        desc940.putEnumerated(idstrokeStyleLineJoinType, idstrokeStyleLineJoinType, idstrokeStyleMiterJoin);
        var idstrokeStyleLineAlignment = stringIDToTypeID("strokeStyleLineAlignment");
        var idstrokeStyleLineAlignment = stringIDToTypeID("strokeStyleLineAlignment");
        var idstrokeStyleAlignInside = stringIDToTypeID("strokeStyleAlignInside");
        desc940.putEnumerated(idstrokeStyleLineAlignment, idstrokeStyleLineAlignment, idstrokeStyleAlignInside);
        var idstrokeStyleScaleLock = stringIDToTypeID("strokeStyleScaleLock");
        desc940.putBoolean(idstrokeStyleScaleLock, false);
        var idstrokeStyleStrokeAdjust = stringIDToTypeID("strokeStyleStrokeAdjust");
        desc940.putBoolean(idstrokeStyleStrokeAdjust, false);
        var idstrokeStyleLineDashSet = stringIDToTypeID("strokeStyleLineDashSet");
        var list85 = new ActionList();
        desc940.putList(idstrokeStyleLineDashSet, list85);
        var idstrokeStyleBlendMode = stringIDToTypeID("strokeStyleBlendMode");
        var idBlnM = charIDToTypeID("BlnM");
        var idNrml = charIDToTypeID("Nrml");
        desc940.putEnumerated(idstrokeStyleBlendMode, idBlnM, idNrml);
        var idstrokeStyleOpacity = stringIDToTypeID("strokeStyleOpacity");
        var idPrc = charIDToTypeID("#Prc");
        desc940.putUnitDouble(idstrokeStyleOpacity, idPrc, 100.000000);
        var idstrokeStyleContent = stringIDToTypeID("strokeStyleContent");
        var desc941 = new ActionDescriptor();
        var idClr = charIDToTypeID("Clr ");
        var desc942 = new ActionDescriptor();
        var idRd = charIDToTypeID("Rd  ");
        desc942.putDouble(idRd, 0.000000);
        var idGrn = charIDToTypeID("Grn ");
        desc942.putDouble(idGrn, 0.000000);
        var idBl = charIDToTypeID("Bl  ");
        desc942.putDouble(idBl, 0.000000);
        var idRGBC = charIDToTypeID("RGBC");
        desc941.putObject(idClr, idRGBC, desc942);
        var idsolidColorLayer = stringIDToTypeID("solidColorLayer");
        desc940.putObject(idstrokeStyleContent, idsolidColorLayer, desc941);
        var idstrokeStyleResolution = stringIDToTypeID("strokeStyleResolution");
        desc940.putDouble(idstrokeStyleResolution, 300.000000);
        var idstrokeStyle = stringIDToTypeID("strokeStyle");
        desc936.putObject(idstrokeStyle, idstrokeStyle, desc940);
        var idcontentLayer = stringIDToTypeID("contentLayer");
        desc935.putObject(idUsng, idcontentLayer, desc936);
        var idLyrI = charIDToTypeID("LyrI");
        desc935.putInteger(idLyrI, 307);
        executeAction(idMk, desc935, DialogModes.NO);
    },
    clearLayers: function() {
        // clear layers
        const doclen = doc.layers.length;
        for(var i = 0; i < doclen; i++) {
            if(i != doclen - 1) {
                doc.layers[0].remove();
            }
        }
    },
    getTextSize: function(layer) {
        const currentActiveLayer = doc.activeLayer;
        doc.activeLayer = layer;
        var size = layer.textItem.size;

        var r = new ActionReference();
        r.putProperty(stringIDToTypeID("property"), stringIDToTypeID("textKey"));
        r.putEnumerated(stringIDToTypeID("layer"), stringIDToTypeID("ordinal"), stringIDToTypeID("targetEnum"));
        
        var yy = 4;
        var yx = 0;
        
        try {
            var transform = executeActionGet(r).getObjectValue(stringIDToTypeID("textKey")).getObjectValue(stringIDToTypeID("transform"));
            yy = transform.getDouble(stringIDToTypeID("yy"));
            yx = transform.getDouble(stringIDToTypeID("yx"));
        } catch (e) {
            // alert(e);
        }

        var coeff = Math.sqrt(yy * yy + yx * yx);
        doc.activeLayer = currentActiveLayer;
        return size * coeff;
    }
};

function generateSkeleton() {
    try {
        if (!(doc.layers.length == 1 && doc.layers[0].name == 'Background')) 
            return alert('Not a new file!');
        
        const structure = {
            layers: [
                {
                    name: 'texture',
                    type: 'group',
                    layers: [
                        // {
                        //     name: 'some texture',
                        //     type: 'layer'
                        // }
                    ]
                },
                {
                    name: 'song',
                    type: 'group',
                    layers: [
                        {
                            name: 'title',
                            type: 'text'
                        }, {
                            name: 'box',
                            type: 'layer'
                        }, {
                            name: 'lyrics',
                            type: 'text'
                        }, {
                            name: 'mask',
                            type: 'vector'
                        },
                    ]
                },
                {
                    name: 'texts',
                    type: 'group',
                    layers: [
                        // {
                        //     name: 'demo text 1',
                        //     type: 'group',
                        //     layers: [
                        //         {
                        //             name: 'Text Label Goes Here',
                        //             type: 'text'
                        //         }, {
                        //             name: 'box',
                        //             type: 'layer'
                        //         }
                        //     ]
                        // }, {
                        //     name: 'demo text 2*',
                        //     type: 'group',
                        //     layers: [
                        //         {
                        //             name: 'Text Label Goes Here',
                        //             type: 'text'
                        //         }, {
                        //             name: 'box',
                        //             type: 'layer'
                        //         }
                        //     ]
                        // }
                    ]
                },
                {
                    name: 'maps',
                    type: 'group',
                    layers: [
                        // {
                        //     name: 'demo map 1',
                        //     type: 'group',
                        //     layers: [
                        //         {
                        //             name: 'marker',
                        //             type: 'layer'
                        //         }, {
                        //             name: 'mask',
                        //             type: 'vector'
                        //         }
                        //     ]
                        // }
                    ]
                }, {
                    name: 'photos',
                    type: 'group',
                    layers: [
                        // {
                        //     name: 'demo photo',
                        //     type: 'vector'
                        // }
                    ]
                }, {
                    name: 'backgroundImages',
                    type: 'group',
                    layers: [
                        {
                            name: 'demo background 1',
                            type: 'group'
                        }, 
                        // {
                        //     name: 'demo background 2',
                        //     type: 'layer'
                        // }
                    ]
                }, {
                    name: 'backgroundColors',
                    type: 'group',
                    layers: [
                        // {
                        //     name: 'demo color 1',
                        //     type: 'layer'
                        // }
                    ]
                },
            ]
        };
        function createLayers(config, path) {
            var node = doc;
            if (! path) 
                path = [];
             else {
                for (var i = 0; i < path.length; i++) {
                    node = node.layerSets.getByName(path[i]);
                }
            }
            for (var i = 0; i < config.layers.length; i++) {
                var layerConfig = config.layers[config.layers.length - i - 1];
                if (layerConfig.type == 'group') {
                    node.layerSets.add().name = layerConfig.name;
                    if (layerConfig.layers) {
                        createLayers(layerConfig, path.concat([layerConfig.name]));
                    }
                } else if (layerConfig.type == 'layer') {
                    node.artLayers.add().name = layerConfig.name;
                } else if (layerConfig.type == 'text') {
                    var newLayer = node.artLayers.add();
                    newLayer.name = layerConfig.name;
                    newLayer.kind = LayerKind.TEXT;
                } else if (layerConfig.type == 'vector') {
                    var newLayer = node.artLayers.add();
                    newLayer.name = layerConfig.name;
                    doc.activeLayer = newLayer;
                    helper.drawSomeShape();
                }
            }
        }
        createLayers(structure);
        doc.layers.getByName('Background').remove();
    } catch (e) {
        alert(e);
        alert('Error at generateSkeleton()');
    }
}

function saveBackgrounds() {
    try {
        const backupState = helper.backupVisibilityState();
        const customBgLayers = helper.getLayer(doc.layerSets, 'backgroundImages');
        if (customBgLayers) {
            for (var i = 0; i < customBgLayers.layers.length; i++) {
                var customBgLayer = customBgLayers.layers[i];
                helper.hideAll();
                helper.showAll(customBgLayer);
                customBgLayers.visible = true;
                customBgLayer.visible = true;
                doc.saveAs(File(doc.path + "/../images/background_" + i + ".png"), pngOpts, true);
                app.open(File(doc.path + "/../images/background_" + i + ".png"));
                activeDocument.width > activeDocument.height ? activeDocument.resizeImage(1500, null, 72, ResampleMethod.NEARESTNEIGHBOR) : activeDocument.resizeImage(null, 1500, 72, ResampleMethod.NEARESTNEIGHBOR);
                activeDocument.saveAs(File(doc.path + "/../images/preview-background_" + i + ".png"), pngOpts, true);
                activeDocument.close(SaveOptions.DONOTSAVECHANGES);
            }
        }
        var textureCount = 0;
        for (var i = 0; i < doc.layerSets.length; i++) {
            var layer = doc.layerSets[doc.layerSets.length - i - 1];
            if (layer.name == 'texture') {
                helper.hideAll();
                helper.showAll(layer);
                layer.visible = true;
                doc.saveAs(File(doc.path + '/../images/texture_' + textureCount + '.png'), pngOpts, true);
                app.open(File(doc.path + '/../images/texture_' + textureCount + '.png'));
                activeDocument.width > activeDocument.height ? activeDocument.resizeImage(1500, null, 72, ResampleMethod.NEARESTNEIGHBOR) : activeDocument.resizeImage(null, 1500, 72, ResampleMethod.NEARESTNEIGHBOR);
                activeDocument.saveAs(File(doc.path + '/../images/preview-texture_' + textureCount + '.png'), pngOpts, true);
                activeDocument.close(SaveOptions.DONOTSAVECHANGES);
                textureCount++;
            }
        }
        helper.restoreVisibilityState(backupState);
        alert('OKKK!');
    } catch (e) {
        alert(e);
        alert('Error at saveBackgrounds()');
    }
}

function saveMarkers() {
    try {
        const mapLayers = helper.getLayer(doc.layerSets, 'maps');
        if (! mapLayers) 
            return;
        
        const activeLayerBackup = doc.activeLayer;
        for (var i = 0; i < mapLayers.layerSets.length; i++) {
            var layer = mapLayers.layerSets[i];
            for (var j = 0; j < layer.layers.length; j++) {
                var sublayer = layer.layers[j];
                if (sublayer.name == 'marker') {
                    doc.activeLayer = sublayer;
                    helper.openActiveLayer();
                    activeDocument.saveAs(File(doc.path + "/../images/map-marker_" + i + ".png"), pngOpts, true);
                    activeDocument.close(SaveOptions.DONOTSAVECHANGES);
                }
            }
        }
        doc.activeLayer = activeLayerBackup;
        alert('OKKK!');
    } catch (e) {
        alert(e);
        alert('Lỗi có thể do marker không phải là Smart Object!');
    }
}

function processCustomBgColors() {
    try {
        const backupState = helper.backupVisibilityState();
        const activeLayerBackup = doc.activeLayer;
        const customBgColorsLayer = helper.getLayer(doc.layerSets, 'backgroundColors');
        for (var i = 0; i < customBgColorsLayer.layers.length; i++) {
            var sublayer = customBgColorsLayer.layers[i];
            helper.hideAll();
            doc.activeLayer = sublayer;
            customBgColorsLayer.visible = true;
            sublayer.visible = true;
            var color = helper.colorSampler(doc.width / 2, doc.height / 2);
            sublayer.name = color;
        }
        doc.activeLayer = activeLayerBackup;
        helper.restoreVisibilityState(backupState);
        alert('OKKK! Nếu có cái cursor ở giữa thì bấm Ctrl + H một vài lần để tắt nhé!');
    } catch (e) {
        alert(e);
        alert('Lỗi có thể do không select được màu trong background color layer! Nếu có cái cursor ở giữa thì bấm Ctrl + H một vài lần để tắt nhé!');
    }
}

function processDefaultBackground() {
    try {
        const backgroundLayers = helper.getLayer(doc.layerSets, 'backgroundImages');
        if (! backgroundLayers || ! backgroundLayers.layers.length) {
            alert('Dù không cần background cũng nên thêm một layer background mặc định trong group "backgroundImages" nhé!');
            return [];
        }
        const data = [];
        var layerinfo = helper.getLayerDimensionsInfo(backgroundLayers.layers[0]);
        layerinfo.left = 0;
        layerinfo.top = 0;
        layerinfo.width = doc.width.value;
        layerinfo.height = doc.height.value;
        layerinfo.type = 'fg';
        layerinfo.image = 'images/background_0.png';
        layerinfo.preview = 'images/preview-background_0.png';
        data.push(layerinfo);
        return data;
    } catch (e) {
        alert(e);
        alert('Error at processDefaultBackground()');
        return [];
    }
}

function processCustomBackgrounds() {
    try {
        const customBackgroundColors = [];
        const customBgColorsLayer = helper.getLayer(doc.layerSets, 'backgroundColors');
        const customBgImagesLayer = helper.getLayer(doc.layerSets, 'backgroundImages');
        const data = {
            left: 0,
            top: 0,
            width: doc.width.value,
            height: doc.height.value,
            opacity: 1,
            visible: true,
            blendMode: 'normal',
            type: '',
            images: {}
        };
        if(customBgImagesLayer) {
            for (var i = 0; i < customBgImagesLayer.layers.length; i++) {
                data.images['custombg_' + i] = 'product/images/preview-background_' + i + '.png';
                customBackgroundColors.push('$custombg_' + i);
            }
        }
        if(customBgColorsLayer) {
            for (var i = 0; i < customBgColorsLayer.layers.length; i++) {
                var sublayer = customBgColorsLayer.layers[i];
                customBackgroundColors.push(sublayer.name);
            }
        }
        data.type = 'bgcolor[label:Choose Background,clickable:true,color:ffffff00,changecolor:' + customBackgroundColors.join('/') + ']';
        return [data];
    } catch (e) {
        alert(e);
        alert('Error at processCustomBackgrounds()');
        return [];
    }
}

function processTexts() {
    try {
        const textLayers = helper.getLayer(doc.layerSets, 'texts');
        if (! textLayers) 
            return [];
        
        const data = [];
        var fontSizeAlerted = false;
        for (var i = 0; i < textLayers.layers.length; i++) {
            var layer = textLayers.layers[i];
            var font = {};
            var textContent = '';
            var label = '';
            var required = layer.name.indexOf('*') != -1 ? 'true' : 'false';
            var layerinfo = null;
            for (var j = 0; j < layer.layers.length; j++) {
                var sublayer = layer.layers[j];
                if (sublayer.kind == LayerKind.TEXT) {
                    textContent = sublayer.textItem.contents;
                    label = sublayer.name;
                    font.name = sublayer.textItem.font;
                    var fontsize = 160;
                    try {
                        var unit = ('' + sublayer.textItem.size).match(/\s.+$/g);
                        if (unit != ' pt' && ! fontSizeAlerted) {
                            alert('Unsupported font size unit:' + unit);
                            fontSizeAlerted = true;
                        }
                        fontsize = sublayer.textItem.size.value * 16;
                    }
                    catch(e) {
                        alert('Không lấy được font size của font ' + sublayer.textItem.font + ' nên đặt giá trị mặc định là 160.');
                    }
                    font.sizes = [fontsize];
                    font.path = 'fonts/' + encodeURI(font.name) + '.ttf';
                    font.colors = [[
                        Math.round(sublayer.textItem.color.rgb.red),
                        Math.round(sublayer.textItem.color.rgb.green),
                        Math.round(sublayer.textItem.color.rgb.blue),
                        Math.round(sublayer.opacity / 100 * 255)
                    ]];
                    font.alignment = ['center'];
                } else if (sublayer.name == 'box') {
                    layerinfo = helper.getLayerDimensionsInfo(sublayer);
                }
            }
            if (layerinfo == null) {
                alert('Text "' + layer.name + '" box not found!');
                layerinfo = {};
            }
            layerinfo.type = 'text[required:' + required + ',label:' + label + ',valign:top]';
            layerinfo.lineHeight = 1;
            layerinfo.text = {
                value: textContent,
                font: font,
                box: {
                    width: layerinfo.width,
                    height: layerinfo.height
                }
            };
            data.push(layerinfo);
        }
        return data;
    } catch (e) {
        alert(e);
        alert('Error at processTexts()');
        return [];
    }
}

function processMask() {
    try {
        const maskLayers = helper.getLayer(doc.layerSets, 'photos');
        if (! maskLayers) 
            return [];
        
        const data = [];
        const activeLayerBackup = doc.activeLayer;
        for (var i = 0; i < maskLayers.layers.length; i++) {
            var layer = maskLayers.layers[i];
            if (layer.kind == LayerKind.SOLIDFILL) {
                var layerinfo = helper.getLayerDimensionsInfo(layer);
                doc.activeLayer = layer;
                layerinfo.type = 'mask';
                layerinfo.image = 'images/photo-mask_' + i + '.svg';
                writeText(doc.path + '/../images/photo-mask_' + i + '.svg', helper.getActiveLayerSVG());
                data.push(layerinfo);
            }
        }
        doc.activeLayer = activeLayerBackup;
        return data;
    } catch (e) {
        alert(e);
        alert('Error at processMask()');
        return [];
    }
}

function processMap() {
    try {
        const mapLayers = helper.getLayer(doc.layerSets, 'maps');
        if (! mapLayers) 
            return [];
        
        const data = [];
        const masks = [];
        const markers = [];
        const mapboxes = [];
        const activeLayerBackup = doc.activeLayer;
        for (var i = 0; i < mapLayers.layerSets.length; i++) {
            var layer = mapLayers.layerSets[i];
            var maskInfo = null;
            var markerInfo = null;
            for (var j = 0; j < layer.layers.length; j++) {
                var sublayer = layer.layers[j];
                if (sublayer.kind == LayerKind.SOLIDFILL && sublayer.name == 'mask') {
                    doc.activeLayer = sublayer;
                    maskInfo = helper.getLayerDimensionsInfo(sublayer);
                    writeText(doc.path + '/../images/map-mask_' + i + '.svg', helper.getActiveLayerSVG());
                } else if (sublayer.name == 'marker') {
                    markerInfo = helper.getLayerDimensionsInfo(sublayer);
                    doc.activeLayer = sublayer;
                    helper.openActiveLayer();
                    var scale = markerInfo.height / activeDocument.height.value;
                    markerInfo.width = activeDocument.width.value * scale;
                    markerInfo.height = activeDocument.height.value * scale;
                    activeDocument.close(SaveOptions.DONOTSAVECHANGES);
                }
            }
            if (maskInfo == null) {
                alert('Map "' + layer.name + '" mask not found!');
                maskInfo = {
                    width: 0,
                    height: 0
                };
            }
            if (markerInfo == null) {
                alert('Map "' + layer.name + '" marker not found!');
                markerInfo = {
                    width: 0,
                    height: 0
                };
            }
            var mapboxInfo = JSON.parse(JSON.stringify(maskInfo));
            var x = parseInt((maskInfo.width - markerInfo.width) / 2);
            var y = parseInt((maskInfo.height - markerInfo.height) / 2);
            maskInfo.type = 'map-mask[id:' + i + ',x:' + x + ',y:' + y + ',index:1,label:' + layer.name + ']',
            maskInfo.image = 'images/map-mask_' + i + '.svg';
            masks.push(maskInfo);
            markerInfo.type = 'marker[id:' + i + ']';
            markerInfo.image = 'images/map-marker_' + i + '.png';
            markers.push(markerInfo);
            mapboxInfo.type = "map[id:" + i + ",style:mapbox-ckhzqo9bu1ybf19pu59w56it6,zoom:11,align:middle]";
            mapboxes.push(mapboxInfo);
        }
        data = [].concat(masks).concat(markers).concat(mapboxes);
        doc.activeLayer = activeLayerBackup;
        return data;
    } catch (e) {
        alert(e);
        alert('Error at processMap()');
        return [];
    }
}

function processSong() {
    try {
        const songLayer = helper.getLayer(doc.layerSets, 'song');
        if (! songLayer) 
            return [];
        
        const data = [];
        const activeLayerBackup = doc.activeLayer;
        var titleLayer = null;
        var boxLayer = null;
        var lyricsLayer = null;
        var maskLayer = null;
        var titleInfo = {};
        var lyricsInfo = {};
        for (var i = 0; i < songLayer.layers.length; i++) {
            var sublayer = songLayer.layers[i];
            if (sublayer.name == 'title') {
                titleLayer = sublayer;
            } else if (sublayer.name == 'box') {
                boxLayer = sublayer;
            } else if (sublayer.name == 'lyrics') {
                lyricsLayer = sublayer;
            } else if (sublayer.name == 'mask') {
                maskLayer = sublayer;
            }
        }
        if (titleLayer == null) {
            alert('Song title not found!');
            return [];
        }
        if (boxLayer == null) {
            alert('Song title box not found!');
            return [];
        }
        if (lyricsLayer == null) {
            alert('Song lyrics not found!');
            return [];
        }
        if (maskLayer == null) {
            alert('Song lyrics mask not found!');
            return [];
        }
        titleInfo = helper.getLayerDimensionsInfo(boxLayer);
        titleInfo.type = "text[module:song_title,valign:top,required:false]";
        titleInfo.text = {
            value: titleLayer.textItem.contents,
            font: {
                name: titleLayer.textItem.font,
                sizes: [titleLayer.textItem.size.value * 16],
                colors: [
                    [
                        Math.round(titleLayer.textItem.color.rgb.red),
                        Math.round(titleLayer.textItem.color.rgb.green),
                        Math.round(titleLayer.textItem.color.rgb.blue),
                        Math.round(titleLayer.opacity / 100 * 255)
                    ]
                ],
                alignment: ['center'],
                path: 'fonts/' + titleLayer.textItem.font + '.ttf'
            },
            box: {
                width: titleInfo.width,
                height: titleInfo.height
            }
        };
        lyricsInfo = helper.getLayerDimensionsInfo(maskLayer);
        lyricsInfo.type = "text[module:song_lyrics,required:false]";
        lyricsInfo.text = {
            value: lyricsLayer.textItem.contents,
            font: {
                name: lyricsLayer.textItem.font,
                sizes: [lyricsLayer.textItem.size.value * 16],
                colors: [
                    [
                        Math.round(lyricsLayer.textItem.color.rgb.red),
                        Math.round(lyricsLayer.textItem.color.rgb.green),
                        Math.round(lyricsLayer.textItem.color.rgb.blue),
                        Math.round(lyricsLayer.opacity / 100 * 255)
                    ]
                ],
                alignment: ['left'],
                path: 'fonts/' + lyricsLayer.textItem.font + '.ttf'
            },
            box: {
                width: lyricsInfo.width,
                height: lyricsInfo.height
            }
        };
        doc.activeLayer = maskLayer;
        writeText(doc.path + '/../images/song-mask.svg', helper.getActiveLayerSVG());
        data = [{
                left: lyricsInfo.left,
                top: lyricsInfo.top,
                width: lyricsInfo.width,
                height: lyricsInfo.height,
                opacity: 1,
                visible: true,
                type: "song-mask",
                image: "images/song-mask.svg"
            }, titleInfo, lyricsInfo];
        doc.activeLayer = activeLayerBackup;
        return data;
    } catch (e) {
        alert(e);
        alert('Error at processSong()');
        return [];
    }
}

function processTexture(layer, textureId) {
    const data = {
        left: 0,
        top: 0,
        width: doc.width.value,
        height: doc.height.value,
        opacity: layer.opacity / 100,
        visible: true,
        blendMode: 'normal',
        type: 'fg'
    };
    data.image = 'images/texture_' + textureId + '.png';
    data.preview = 'images/preview-texture_' + textureId + '.png';
    return [data];
}

function exportData() {
    try {
        if (doc.name.indexOf('.psd') == -1 && doc.name.indexOf('.psb') == -1)
            return alert('Lỗi: file chưa được lưu trên ổ cứng!');
        
        const datajson = {
            document: {
                width: doc.width.value,
                height: doc.height.value,
                resolution: doc.resolution
            },
            children: processDefaultBackground()
        };
        var textureCount = 0;
        var customBgProcessed = false;
        for (var i = 0; i < doc.layerSets.length; i++) {
            var layerSet = doc.layerSets[doc.layerSets.length - i - 1];
            if (layerSet.name == 'song') 
                datajson.children = datajson.children.concat(processSong());
            
            if (layerSet.name == 'texts') 
                datajson.children = datajson.children.concat(processTexts());
            
            if (layerSet.name == 'maps') 
                datajson.children = datajson.children.concat(processMap());
            
            if (layerSet.name == 'photos') 
                datajson.children = datajson.children.concat(processMask());
            
            if ((layerSet.name == 'backgroundImages' || layerSet.name == 'backgroundColors') && ! customBgProcessed) {
                customBgProcessed = true;
                datajson.children = datajson.children.concat(processCustomBackgrounds());
            }
            if (layerSet.name == 'texture') {
                datajson.children = datajson.children.concat(processTexture(layerSet, textureCount));
                textureCount++;
            }
        }
        writeJSON(doc.path + '/../data.json', datajson);
        alert('OKKK!');
    } catch (e) {
        alert(e);
    }
}

function test() {
    try {
    }
    catch(e) {
        alert(e);
    }
}
box.show();
