
const cwd = new File($.fileName).parent;
loadScript = function loadScript(path) {
    try {
        try {
            // relative path
            $.evalFile(cwd + "/" + path);
        } catch (e) {
            // absolute path
            $.evalFile(path);
        }
    }
    catch(e) {
        alert('Error while importing scripts "' + path + '": ' + e);
    }
}


loadScript('import/json2.js');
loadScript('import/prototype.js');
loadScript('import/save_options.js');
loadScript('import/helper.js');


